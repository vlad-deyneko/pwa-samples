const CACHE_NAME = 'crystal-pwa-cache';
const timeout = 400;

var urlsToCache = [
    'style.css',
    '/js/script1.js',
    '/js/script2.js',
    '/js/script3.js',
    '/js/script4.js',
    '/images/jurassic-park-001.gif',
    '/images/jurassic-park-002.gif',
    '/images/jurassic-park-003.gif',
    '/images/jurassic-park-004.gif',
    '/images/jurassic-park-005.gif',
    '/images/jurassic-park-006.gif',
    '/images/jurassic-park-007.gif',
    '/images/jurassic-park-008.gif'
];

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function (cache) {
                console.log('Opened cache.');
                return cache.addAll(urlsToCache);
            })
    );
    console.log('Service Worker has been installed in event type: ', event.type);
});

// On fetch, send request to network, but on timeout - fetch data from cache
self.addEventListener('fetch', (event) => {
    event.respondWith(
        fromNetwork(event.request, timeout)
        .catch((error) => {
            console.log(`Error: ${error.message()}`);
            return fromCache(event.request);
        }));
});

// Network request with timeout
function fromNetwork(request, timeout) {
    return new Promise((fulfill, reject) => {
        var timeoutId = setTimeout(reject, timeout);

        fetch(request).then((response) => {
            clearTimeout(timeoutId);
            fulfill(response);
        }, reject);
    });
}

function fromCache(request) {
    // Open cache (CacheStorage API), fetch matching resources
    // If value was not found - Promise will succeed, but with the 'undefined' value
    return caches.open(CACHE_NAME).then((cache) =>
        cache.match(request).then((matching) =>
            matching || Promise.reject('no-match')
        )
    );
}
