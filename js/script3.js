(function () {
    angular.module('storeApp', ['fwcFilters', 'fwcControllers', 'ngMessages', 'ngStorage', 'matchmedia-ng']);
})();
(function () {
    var app = angular.module('fwcControllers', ['fwcDirectives', 'fwcServices']);

    app.controller('ToplinksController', ['$scope', '$attrs', '$timeout', 'sidebarService', 'cartService',
        function ($scope, $attrs, $timeout, sidebarService, cartService) {
            var ae = angular.element;

            $scope.loadCart = function ($event) {
                $event.preventDefault();
                var url = ae($event.currentTarget).attr('href');
                cartService.load(url);
            };

        }
    ]);

    app.controller('SidebarController', ['$scope', '$element', 'sidebarService',
        function ($scope, $element, sidebarService) {
            var ae = angular.element;

            this.active = function () {
                if (sidebarService.isOpen)
                    ae('body').addClass('sidebar-expand');
                else
                    ae('body').removeClass('sidebar-expand');

                return sidebarService.isOpen ? 'expand' : '';
            };

            this.showContent = function (section) {
                return sidebarService.activeContent === section;
            };

            this.close = function () {
                sidebarService.close();
            };
        }
    ]);

    app.controller('CartController', ['$scope', 'cartService',
        function ($scope, cartService) {
            var ae = angular.element;

            this.getProducts = function () {
                return cartService.products;
            };

            this.isEmpty = function () {
                return cartService.empty;
            };

            this.getShipping = function () {
                return cartService.shipping;
            };

            this.setLoader = function (bool) {
                cartService.setLoader(bool);
            };

            this.getLoader = function () {
                return cartService.loader;
            };

            $scope.remove = function ($event) {
                $event.preventDefault();
                cartService.remove(ae($event.target));
            };

            $scope.priceExist = function($ruleprice,$specialprice){
                console.log($ruleprice,$specialprice);
                if($ruleprice){
                    console.log('rulePrice -> '+$ruleprice);
                    return true;
                } else if ($specialprice){
                    console.log('specialprice -> '+$specialprice);
                    return true;
                } else {
                    console.log('normal Price');
                    return false;
                }
            }
        }
    ]);

    app.controller('matchCosmetics', ['$scope', 'matchCosmeticsService', function ($scope, matchCosmeticsService) {

        $scope.resetFaceOptions = function () {

            matchCosmeticsService.setResetFaceMapFromStorage();
            matchCosmeticsService.resetMatchCosmeticsFaceMapJson();
        };
    }]);

    app.controller('checkoutController', ['$scope', function ($scope) {

        var ae = angular.element;

        $scope.billingHeader = null;
        $scope.shippingHeader = null;
        $scope.billingAddress = {
            'firstname': null, 'lastname': null, 'street1': null, 'street2': null, 'street3': null, 'city': null, 'postcode': null, 'telephone': null, 'email': null, 'company': null, 'vat': null
        };
        $scope.shippingAddress = {
            'firstname': null, 'lastname': null, 'street1': null, 'street2': null, 'street3': null, 'city': null, 'postcode': null, 'telephone': null
        };
        $scope.settings = false;
        $scope.settings.billingAddresses = true;
        $scope.settings.shippingAddresses = true;
        $scope.settings.sameAsBilling = false;


        $scope.$watchGroup(['billingAddress.firstname', 'billingAddress.lastname', 'billingAddress.street1',
            'billingAddress.street2', 'billingAddress.street3', 'billingAddress.postcode', 'billingAddress.city', 'billingAddress.telephone', 'billingAddress.email', 'billingAddress.company', 'billingAddress.vat'], function () {

            var arrayIsEmpty = false;
            angular.forEach($scope.billingAddress, function (value) {
                if (value !== null) {
                    arrayIsEmpty = true;
                }
            });

            if (!arrayIsEmpty) {
                $scope.billingHeader = false;
            } else {
                $scope.billingHeader = true;
            }

        });

        $scope.billingTitle = function (model) {

            if (model) {
                $scope.billingHeader = true;
            }
        };

        $scope.billingFirstnameBlur = function (model) {
            if (model) {
                $scope.billingAddress.firstname = model;
            } else {
                $scope.billingAddress.firstname = null;
            }
        };

        $scope.billingLastnameBlur = function (model) {
            if (model) {
                $scope.billingAddress.lastname = model;
            } else {
                $scope.billingAddress.lastname = null;
            }

        };

        $scope.billingStreet1Blur = function (model) {
            if (model) {
                $scope.billingAddress.street1 = model;
            } else {
                $scope.billingAddress.street1 = null;
            }
        };

        $scope.billingStreet2Blur = function (model) {
            if (model) {
                $scope.billingAddress.street2 = model;
            } else {
                $scope.billingAddress.street2 = null;
            }
        };

        $scope.billingStreet3OnChange = function (model) {
            if ($scope.billingAddress.street3) {
                $scope.billingAddress.street3 = model;
                ae('#preview-scope span.billing-street-sep').removeClass('hidden');
            } else {
                $scope.billingAddress.street3 = null;
                ae('#preview-scope span.billing-street-sep').addClass('hidden');
            }
        };

        $scope.billingCityOnChange = function (model) {
            if ($scope.billingAddress.city) {
                $scope.billingAddress.city = model;
                $scope.billingAddress.country = ae('select[name="billing[country_id]"').text();
                ae('#preview-scope span.billing-city-sep').removeClass('hidden');
            } else {
                $scope.billingAddress.city = null;
                $scope.billingAddress.country = null;
                ae('#preview-scope span.billing-city-sep').addClass('hidden');
            }
        };

        $scope.billingPostcodeBlur = function (model) {
            if (model) {
                $scope.billingAddress.postcode = model;
            } else {
                $scope.billingAddress.postcode = null;
            }
        };

        $scope.billingCityBlur = function (model) {
            if (model) {
                $scope.billingAddress.city = model;
            } else {
                $scope.billingAddress.city = null;
            }
        };


        $scope.billingTelephoneBlur = function (model) {
            if (model) {
                $scope.billingAddress.telephone = model;
            } else {
                $scope.billingAddress.telephone = null;
            }
        };

        $scope.billingEmailBlur = function (model) {
            if (model) {
                $scope.billingAddress.email = model;
            } else {
                $scope.billingAddress.email = null;
            }
        };

        $scope.billingCompanyBlur = function (model) {
            if (model) {
                $scope.billingAddress.company = model;
            } else {
                $scope.billingAddress.company = null;
            }
        };

        $scope.billingVatBlur = function (model) {
            if (model) {
                $scope.billingAddress.vat = model;
            } else {
                $scope.billingAddress.vat = null;
            }
        };

        $scope.billingVatOnChange = function (model) {
            if ($scope.billingAddress.vat) {
                $scope.billingAddress.vat = model;
                ae('#preview-scope span.billing-vat-prefix').removeClass('hidden');
            } else {
                $scope.billingAddress.vat = null;
                ae('#preview-scope span.billing-vat-prefix').addClass('hidden');
            }
        };

        $scope.billingAddressesOnChange = function (model) {
            if (model) {
                $scope.settings.billingAddresses = false;
                ae('#preview-scope .billing-address').addClass('hidden');
                ae('#preview-scope #preview-billing-address-' + model + ' h4.billing-title').removeClass('hidden');
                ae('#preview-scope #preview-billing-address-' + model).removeClass('hidden');
            } else {
                ae('#preview-scope .billing-address').addClass('hidden');
                $scope.settings.billingAddresses = true;
            }
        };

        $scope.shippingAddressesOnChange = function (model) {
            if (model) {
                $scope.settings.shippingAddresses = false;
                ae('#preview-scope .shipping-address').addClass('hidden');
                ae('#preview-scope #preview-shipping-address-' + model + ' h4.shipping-title').removeClass('hidden');
                ae('#preview-scope #preview-shipping-address-' + model).removeClass('hidden');
            } else {
                ae('#preview-scope .shipping-address').addClass('hidden');
                $scope.settings.shippingAddresses = true;
            }
        };

        $scope.sameAsBillingClick = function (model) {
            if (model) {
                $scope.shippingAddress = angular.copy($scope.billingAddress);
            } else {
                $scope.shippingAddress = null;
            }
        };

        //

        $scope.$watchGroup(['shippingAddress.firstname', 'shippingAddress.lastname', 'shippingAddress.street1',
            'shippingAddress.street2', 'shippingAddress.street3', 'shippingAddress.postcode', 'shippingAddress.city', 'shippingAddress.telephone'], function () {

            var arrayIsEmpty = false;

            angular.forEach($scope.shippingAddress, function (value) {
                if (value !== null) {
                    arrayIsEmpty = true;
                }
            });

            if (!arrayIsEmpty) {
                $scope.shippingHeader = false;
            } else {
                $scope.shippingHeader = true;
            }
        });

        $scope.shippingTitle = function (model) {

            if (model) {
                $scope.shippingHeader = true;
            }
        };

        $scope.shippingFirstnameBlur = function (model) {
            if (model) {
                $scope.shippingAddress.firstname = model;
            } else {
                $scope.shippingAddress.firstname = null;
            }
        };

        $scope.shippingLastnameBlur = function (model) {
            if (model) {
                $scope.shippingAddress.lastname = model;
            } else {
                $scope.shippingAddress.lastname = null;
            }

        };

        $scope.shippingStreet1Blur = function (model) {
            if (model) {
                $scope.shippingAddress.street1 = model;
            } else {
                $scope.shippingAddress.street1 = null;
            }
        };

        $scope.shippingStreet2Blur = function (model) {
            if (model) {
                $scope.shippingAddress.street2 = model;
            } else {
                $scope.shippingAddress.street2 = null;
            }
        };

        $scope.shippingStreet3OnChange = function (model) {
            if ($scope.shippingAddress.street3) {
                $scope.shippingAddress.street3 = model;
                ae('#preview-scope span.shipping-street-sep').removeClass('hidden');
            } else {
                $scope.shippingAddress.street3 = null;
                ae('#preview-scope span.shipping-street-sep').addClass('hidden');
            }
        };

        $scope.shippingCityOnChange = function (model) {
            if ($scope.shippingAddress.city) {
                $scope.shippingAddress.city = model;
                $scope.shippingAddress.country = ae('select[name="shipping[country_id]"').text();
                ae('#preview-scope span.shipping-city-sep').removeClass('hidden');
            } else {
                $scope.shippingAddress.city = null;
                $scope.shippingAddress.country = null;
                ae('#preview-scope span.shipping-city-sep').addClass('hidden');
            }
        };

        $scope.shippingPostcodeBlur = function (model) {
            if (model) {
                $scope.shippingAddress.postcode = model;
            } else {
                $scope.shippingAddress.postcode = null;
            }
        };

        $scope.shippingCityBlur = function (model) {
            if (model) {
                $scope.shippingAddress.city = model;
            } else {
                $scope.shippingAddress.city = null;
            }
        };


        $scope.shippingTelephoneBlur = function (model) {
            if (model) {
                $scope.shippingAddress.telephone = model;
            } else {
                $scope.shippingAddress.telephone = null;
            }
        };

        $scope.shippingEmailBlur = function (model) {
            if (model) {
                $scope.shippingAddress.email = model;
            } else {
                $scope.shippingAddress.email = null;
            }
        };


        //

        $scope.showTooltip = function () {
            ae('#agreements .tooltip-content').addClass('mouseover');
        };

        $scope.hideTooltip = function () {
            ae('#agreements .tooltip-content').removeClass('mouseover');
        };

    }]);

    app.controller('fromCategoryController', ['$scope', '$compile', '$timeout', function ($scope, $compile, $timeout) {

        $timeout(function () {
            $compile(angular.element('#from-category-carousel .owl-stage .owl-item.cloned'))($scope);
        });
    }]);

    app.controller('facebookApiController', ['$scope', function ($scope) {

        $scope.requestForLogin = function (href) {

            var width = 650;
            var height = 350;

            var frame = null,
                left = parseInt((jQuery(window).width() - width) / 2),
                top = parseInt((jQuery(window).height() - height) / 2);

            var params = [
                'resizable=yes',
                'scrollbars=no',
                'toolbar=no',
                'menubar=no',
                'location=no',
                'directories=no',
                'status=yes',
                'width=' + width,
                'height=' + height,
                'left=' + left,
                'top=' + top
            ];

            if (href) {
                frame = window.open(href, 'facebooklogin_popup', params.join(','));
                frame.focus();

                jQuery(frame.document).ready(function () {

                    var loaderText = 'Loading...';
                    var html = '<!DOCTYPE html><html style="height: 100%;"><head><meta name="viewport" content="width=device-width, initial-scale=1"><title>' + loaderText + '</title></head>';
                    html += '<body style="height: 100%; margin: 0; padding: 0;">';
                    html += '<div style="text-align: center; height: 100%;"><div id="loader" style="top: 50%; position: relative; margin-top: -50px; color: #646464; height:25px; font-size: 25px; text-align: center; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;">' + loaderText + '</div></div>';
                    html += '</body></html>';

                    jQuery(frame.document).contents().html(html);
                });
            }
            return false;
        };
    }]);

    app.controller('loyaltyProgram', ['$scope', '$http', '$log', function ($scope, $http, $log) {
        $scope.url = document.querySelector('.account-register').getAttribute("data-url");
        $scope.response = [];
        $http.get($scope.url)
            .success(function (data) {

                // Create Loader
                jQuery(document).ready(function () {
                    jQuery('body.fwc-loyaltyportal-customer-edit').append('<div class="kontigo-loader"></div>');
                });

                $scope.response = data;
                var date = $scope.response.DateOfBirth.split('-'),
                    gender = $scope.response.Gender.toLowerCase();

                // Birthday
                $scope.year = date[0];
                $scope.month = date[1];
                $scope.day = date[2];

                // Gender
                document.getElementById(gender).checked = true;

                /* Customer Properties */
                if($scope.response.CustomerProperties.length){ // && $scope.response.PromotionCounters.length = zwracalo null

                    // Consents

                    // TODO: refactor this shame code...
                    document.querySelector('#example-1').checked = $scope.response.PersonalDataProcessingConsent;
                    document.querySelector('#example-2').checked = $scope.response.EmailCommunicationConsent;
                    document.querySelector('#example-3').checked = $scope.response.SmsCommunicationConsent;
                    document.querySelector('#example-4').checked = $scope.response.AlcoholMarketingConsent;
                    // TODO: ^^^^

                    // hair
                    document.querySelector('.'+$scope.response.CustomerProperties[0].Name+ "-" + $scope.response.CustomerProperties[0].Value +"").checked = true;

                    // eyes
                    document.querySelector('.'+$scope.response.CustomerProperties[1].Name+ "-" + $scope.response.CustomerProperties[1].Value +"").checked = true;

                    // others
                    $scope.response.CustomerProperties.forEach(function (element) {
                        if (element.Name !== 'EyeColor' && element.Name !== 'HairColor' && element.Value !== null) {
                            document.querySelector("[value="+[element.Value]+"]").checked = true;

                            var season = kontigo.ifContainsText(element.Name, 'FavouriteSeason'),
                                category = kontigo.ifContainsText(element.Name, 'FavouriteCategory');

                            if(element.Value !== null && season){
                                console.log('season');
                            }

                            if(element.Value !== null && category){
                                console.log('category');
                            }
                        }
                    });
                }

                var options =  {
                    onChange: function(){
                        jQuery('#telephone').mask('(+48) 000-000-000');
                    }
                };

                jQuery('#telephone').mask('(+48) 000-000-000', options);

                var loader = document.querySelector('.default__loader');
                loader.remove();
            });
    }]);

    app.controller('gmailApiController', ['$scope', function ($scope) {

        $scope.requestForLogin = function (href) {

            var width = 650;
            var height = 450;

            var frame = null,
                left = parseInt((jQuery(window).width() - width) / 2),
                top = parseInt((jQuery(window).height() - height) / 2);

            var params = [
                'resizable=yes',
                'scrollbars=no',
                'toolbar=no',
                'menubar=no',
                'location=no',
                'directories=no',
                'status=yes',
                'width=' + width,
                'height=' + height,
                'left=' + left,
                'top=' + top
            ];

            if (href) {
                frame = window.open(href, false, params.join(','));
                frame.focus();

                jQuery(frame.document).ready(function () {

                    var loaderText = 'Loading...';
                    var html = '<!DOCTYPE html><html style="height: 100%;"><head><meta name="viewport" content="width=device-width, initial-scale=1"><title>' + loaderText + '</title></head>';
                    html += '<body style="height: 100%; margin: 0; padding: 0;">';
                    html += '<div style="text-align: center; height: 100%;"><div id="loader" style="top: 50%; position: relative; margin-top: -50px; color: #646464; height:25px; font-size: 25px; text-align: center; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;">' + loaderText + '</div></div>';
                    html += '</body></html>';

                    jQuery(frame.document).contents().html(html);
                });
            }
            return false;
        };
    }]);

    app.controller('gridController', ['$scope', function ($scope) {
        var _cookieName = 'kontigo-grid-mode',
            _defaultValue = 'four-in-line';

        $scope.grid = {
            mode: Mage.Cookies.get(_cookieName) ? Mage.Cookies.get(_cookieName) : _defaultValue
        };
        $scope.$watch('grid.mode', function (value) {
            Mage.Cookies.set(_cookieName, value);
        });
    }]);

    app.controller('promopaperController', ['$scope', '$timeout', function ($scope, $timeout) {
        var _ae = angular.element;
        var _toolbarImages = _ae('.desktop-map[usemap]'),
            _toolbarElement = _ae('#promopaper-toolbar');

        if (!_toolbarImages.length) { return; }

        $scope.promopaper = {
            currentPage: 1,
            pageSum: _toolbarImages.length,
            minValue: 1,
            manualAction: false,
            isZoomed: false,
            gridIsHidden: true,
            modalGrid: '',
            highlightCfg: {
                fillColor: '2297af',
                strokeColor: '2297af'
            },

            init: function () {
                $scope.promopaper.events();
                $scope.promopaper.generateIds();
                $scope.promopaper.modalGrid = $scope.promopaper.getSmallGridNavigationElements();

                jQuery('.desktop-map').maphilight($scope.promopaper.highlightCfg);
                _ae('.zoom-map').addClass('hidden');
                _ae(window).trigger('fixed.bar');
            },
            updateCurrentPageNumber: function (windowElement) {
                if ($scope.promopaper.manualAction) { return; }

                var _tmp = _ae(_toolbarImages),
                    _pageNumber = $scope.promopaper.currentPage;

                for (var i = 0; i < _tmp.length; i++) {
                    if (windowElement.pageYOffset + 125 >= _ae(_tmp[i]).offset().top) {
                        _pageNumber = i + 1;
                    }
                }

                $scope.promopaper.currentPage = _pageNumber;
                $scope.$apply();
            },
            events: function () {
                _ae(window).on('fixed.bar', function () {
                    if(!$scope.promopaper.isZoomed) {
                        $scope.promopaper.fixedToolbar(_toolbarImages.last().offset().top + _toolbarImages.last().height());
                    }
                });
                _ae(window).on('scroll', function () {
                    $scope.promopaper.updateCurrentPageNumber(this);
                    _ae(window).trigger('fixed.bar');
                });
                _ae(window).on('resize', function () {
                    _ae(window).trigger('fixed.bar');
                });
            },
            manualActionDelay: function () {
                $timeout(function () {
                    $scope.promopaper.manualAction = false;
                }, 805);
            },
            fixedToolbar: function (offset) {
                var _windowPosition = _ae(window).scrollTop() + _ae(window).height();
                if (offset > _windowPosition) {
                    _toolbarElement.addClass('fixed');
                } else {
                    _toolbarElement.removeClass('fixed');
                }
            },
            generateIds: function () {
                _toolbarImages.each(function (i) {
                    _ae(this).attr('id', i + 1);
                });
            },
            getSmallGridNavigationElements: function () {
                return _toolbarImages.map(function (i, e) {
                    return _ae('<li>')
                        .addClass('promopaper-grid__item')
                        .append(
                            _ae('<a ng-click="goToPage($event)">')
                                .addClass('promopaper-grid__link')
                                .attr({
                                    'id': i + 1,
                                    'href': '#' + (i + 1)
                                })
                                .css('background-image', 'url(' + _ae(e).attr('src') + ')')
                                .append(
                                    _ae('<span>').addClass('promopaper-grid__page-no').html(i + 1)
                                )
                        );
                }).toArray()
            }
        };

        /* Base functionality initialization */
        $scope.promopaper.init();

        $scope.change = function () {
            if (($scope.promopaper.currentPage > 0) && ($scope.promopaper.currentPage <= $scope.promopaper.pageSum)) {
                $scope.promopaper.manualAction = true;
                kontigo.smoothScroll('#' + $scope.promopaper.currentPage, 800);
                $scope.promopaper.manualActionDelay();
            }
        };

        /* Switcher section */
        $scope.increment = function () {
            if ($scope.promopaper.currentPage >= $scope.promopaper.pageSum) {return; }
            $scope.promopaper.currentPage++;
            $scope.promopaper.manualAction = true;
            kontigo.smoothScroll('#' + $scope.promopaper.currentPage, 800);
            $scope.promopaper.manualActionDelay();
        };
        $scope.decrement = function () {
            if ($scope.promopaper.currentPage <= $scope.promopaper.minValue) { return; }
            $scope.promopaper.currentPage--;
            $scope.promopaper.manualAction = true;
            kontigo.smoothScroll('#' + $scope.promopaper.currentPage, 800);
            $scope.promopaper.manualActionDelay();
        };

        /* Zoom section */
        $scope.zoom = function () {
            if (!$scope.promopaper.isZoomed) {
                $scope.promopaper.isZoomed = true;
                _ae('.desktop-map').addClass('hidden');
                _ae('.zoom-map').removeClass('hidden');
            } else {
                $scope.promopaper.isZoomed = false;
                _ae('.zoom-map').addClass('hidden');
                _ae('.desktop-map').removeClass('hidden');
            }
        };

        /* Small grid navigation section */
        $scope.showGrid = function () {
            $scope.promopaper.gridIsHidden = !$scope.promopaper.gridIsHidden;
        };
        $scope.goToPage = function (event) {
            $scope.showGrid();
            kontigo.smoothScroll('#' + event.target.id, 800);
        };

    }]);

})();


jQuery.noConflict();

(function (jQuery, window, document) {
    'use strict';
    var loaded = false;
    jQuery('body').addClass('category-loyalty-club-popuponly');
    var cappingPopup = {
        fwc_popupStatus: 'fwc_cappingPopup_status',
        fwc_popupClicks: 'fwc_cappingPopup_clicks',
        init: function (numberOfClicks, cookieLifetime) {
            if (cappingPopup.readExistCookie(cappingPopup.fwc_popupStatus) === '1' && (window.matchMedia("(min-width: 769px)").matches)) {
                var clickCountCookie = cappingPopup.readExistCookie(cappingPopup.fwc_popupClicks);
                if (clickCountCookie === '1') {
                    jQuery('#newsletter-popup').modal('show');
                    cappingPopup.eraseCookie(cappingPopup.fwc_popupClicks);
                } else {
                    cappingPopup.updateExistCookie(cappingPopup.fwc_popupClicks, clickCountCookie);
                }
            } else {
                cappingPopup.createNewCookie(cappingPopup.fwc_popupStatus, 1, cookieLifetime);
                cappingPopup.createNewCookie(cappingPopup.fwc_popupClicks, numberOfClicks);
            }
        },
        createNewCookie: function (cookieName, cookieValue, cookieLifetime) {
            var expires;
            if (cookieLifetime) {
                var date = new Date();
                date.setTime(date.getTime() + (cookieLifetime * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            } else {
                expires = "";
            }
            document.cookie = cookieName + '=' + cookieValue + expires + "; path=/";
        },
        readExistCookie: function (cookieName) {
            var name = cookieName + '=';
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
        },
        updateExistCookie: function (cookieName, clickCountCookie) {
            if (clickCountCookie > 0) {
                var updatedCounter = clickCountCookie - 1;
                document.cookie = cookieName + '=' + updatedCounter + "; path=/";
            }
        },
        eraseCookie: function (cookieName) {
            cappingPopup.createNewCookie(cookieName, "", -1);
        }
    };

    window.kontigo = {
        init: function () {
            jQuery('.selectpicker').selectpicker({
                noneSelectedText: 'Nothing selected'
            });
            this.fixForPrototypeAndBootstrap();
            this.showHideAgreement();
            this.showModalMessages();
            this.brandsCarouselInit();
            this.brandsRotatorCarouselInit();
            this.hideFilters();
            this.upsellProducts();
            this.reviewField();
            this.fromCategoryCarouselInit();
            this.homepromotedTabsInit();
            this.categoryPageInit();
            this.addClassToActiveElement();
            this.reviewForm();
            this.nipValidate();
            this.searchInputAutoFocus();
            this.navTabsMenuTimeout();
            this.hideSortByPositionAndRelevance();
            this.baseSliderInit();
            this.rwdImageMaps();
            this.initCustomScroll();
            this.removeCustomTemplateScripts();
            this.menuStyles();
            this.loyaltyClub();
            this.loyaltyClubAjax();
        },

        ifContainsText: function (sentence, word) {
            var reg = new RegExp(word, 'g'),
                element = sentence.match(reg);

            if (element !== null) {
                return element;
            }
        },
        loyaltyPortalPopup: function () {
            if (window.matchMedia("(min-width: 769px)").matches) {
                jQuery('#loyalty-popup').modal({backdrop: 'static', keyboard: false, show: true});
            }
        },
        loyaltyClub: function () {
            jQuery('.customer-account-info .loyalty-program__banner').click(function (e) {
                e.preventDefault();
                jQuery('.loyalty-program__btn').trigger('click');
            });
            this.translations();
        },
        loyaltyClubAjax: function () {
            var bodyClassName = 'categorypath-loyalty-club-html category-loyalty-club';
            var bodySelector = jQuery('body');
            if (bodySelector.hasClass(bodyClassName)) {
                jQuery.ajax({
                    url: '/loyalty-program/customer/checkgroup',
                    type: 'post',
                    dataType: 'json'
                }).done(function (response) {
                    if (response === false) {
                        bodySelector.removeClass(bodyClassName);
                        bodySelector.addClass('category-loyalty-club-popuponly');
                    }
                    kontigo.loyaltyClubAjaxPrice(response);
                }).fail(function () {
                    bodySelector.removeClass(bodyClassName);
                });
            }
        },
        loyaltyClubAjaxPrice: function (parameter) {

            jQuery.ajax({
                url: '/loyalty-program/customer/getprodprices',
                type: 'post',
                dataType: 'json'
            }).done(function (response) {
                //Refresh prices colors for regular users
                if(loaded) return;
                loaded = true;
                if (!parameter) {
                    var productsList = jQuery('.product-item');
                    productsList.children('.price-label').remove();
                    productsList.children('.special-price').toggleClass('special-price regular-price');
                }
                if (typeof response !== 'undefined' && response.length > 0) {

                    response.forEach(function (item) {
                        var productPrice = jQuery('#product-price-' + item.id),
                            oldProductPrice = jQuery('#old-price-' + item.id),
                            specialPriceLabel = jQuery('.price-label'),
                            specialPrice = jQuery('.special-price');

                        //Refresh prices for regular users
                        if (!parameter) {
                            oldProductPrice.remove();
                            specialPriceLabel.remove();
                            specialPrice.toggleClass('special-price regular-price');
                            productPrice.addClass('regular-price');
                            productPrice.children('.price').addClass('active').text(item.base_price.replace(".", ",") + '\xa0zł');
                        }
                        //Refresh prices for Loyalty users
                        if(parameter) {
                            // Replace prices
                            if (productPrice.hasClass('regular-price')) {

                                productPrice.prepend('<span class="old-price">' + item.base_price.replace(".", ",") + '\xa0zł</span>');
                                productPrice.children('.price').text(item.final_price.replace(".", ",") + '\xa0zł').addClass('active');
                                productPrice.children('.old-price').addClass('active');

                            } else {
                                productPrice.text(item.final_price.replace(".", ",") + '\xa0zł').addClass('active');
                                oldProductPrice.text(item.base_price.replace(".", ",") + '\xa0zł');
                            }
                        }
                    });
                }
            })

        },
        // Copied from the admin panel
        translations: function () {
            jQuery('.add-to-cart-box-disabled button span').text('Chwilowo niedostępny');
        },
        menuStyles: function () {
            jQuery('li.level2.parent').closest('ul.level1').addClass('submenu-version');

            jQuery(window).load(function () {
                jQuery('li.level0.active ul.level1').each(function () {
                    var content = jQuery(this),
                        container = content.closest('.content'),
                        contentHeight = content.outerHeight();

                    if (contentHeight > 350) {
                        container.css('height', contentHeight + 80);
                    }
                });
            });
        },
        baseSliderInit: function () {
            var owl = jQuery('#left-slider');
            owl.owlCarousel({
                items: 1,
                loop: true,
                autoplay: true,
                autoplayTimeout: 6000,
                autoplayHoverPause: true,
                smartSpeed: 750,
                dots: true
            });

        },
        categoryPageInit: function () {
            if (jQuery('body').hasClass('catalog-category-view')) {
                this.pagerClick();
            }
        },
        fixForPrototypeAndBootstrap: function () {
            if (Prototype.BrowserFeatures.ElementExtensions) {
                var disablePrototypeJS = function (method, pluginsToDisable) {
                        var handler = function (event) {
                            event.target[method] = undefined;
                            setTimeout(function () {
                                delete event.target[method];
                            }, 0);
                        };
                        pluginsToDisable.each(function (plugin) {
                            jQuery(window).on(method + '.bs.' + plugin, handler);
                        });
                    },
                    pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover', 'tab'];
                disablePrototypeJS('show', pluginsToDisable);
                disablePrototypeJS('hide', pluginsToDisable);
            }
        },
        addClassToActiveElement: function () {
            jQuery('.static-page .nav-left .panel-collapse ul li a').each(function () {
                var _this = jQuery(this),
                    str = _this.attr('href'),
                    patt = new RegExp(window.location.pathname),
                    res = patt.test(str);

                if (res) {
                    _this.parents('.panel-collapse ul>li').addClass('active');
                }
            });
        },
        /**
         * Show a message (global_messages block) when the page is loaded.
         */
        showModalMessages: function () {
            var jQuerymodal = jQuery('#modal-messages');

            if (jQuerymodal) {
                jQuerymodal.modal('show');
            }
        },
        setSortPosition: function (value) {
            if (value !== "") {
                setLocation(value);
            }
        },
        reviewForm: function (value) {
            var dataReviewForm = new VarienForm('review-form');
            Validation.addAllThese(
                [
                    ['validate-rating', Translator.translate('Please select one of each of the ratings above'), function (v) {
                        var ratingsBoxes = jQuery('.ratings .ratings-box'),
                            isValid = true;
                        ratingsBoxes.each(function () {
                            var radioIsChecked = jQuery(this).find("input[type='radio']:checked").val();
                            if (radioIsChecked === undefined) {
                                isValid = false;
                            }
                        });
                        if (isValid) {
                            return true;
                        } else {
                            return false;
                        }

                    }]
                ]
            );
        },
        nipValidate: function () {
            Validation.addAllThese(
                [
                    ['validate-nip', Translator.translate('Wpisany NIP jest nieprawidłowy!'), function (v) {
                        var nipWithoutDashes = v.replace(/-/g,"");
                        var reg = /^[0-9]{10}$/;
                        if(parseInt(nipWithoutDashes) === 0) {
                            return false;
                        }
                        if(reg.test(nipWithoutDashes) === false) {
                            return false;}
                        else
                        {
                            var digits = (""+nipWithoutDashes).split("");
                            var checksum = (6*parseInt(digits[0]) + 5*parseInt(digits[1]) + 7*parseInt(digits[2]) + 2*parseInt(digits[3]) + 3*parseInt(digits[4]) + 4*parseInt(digits[5]) + 5*parseInt(digits[6]) + 6*parseInt(digits[7]) + 7*parseInt(digits[8]))%11;

                            return (parseInt(digits[9]) === checksum);
                        }
                    }]
                ]
            );
        },
        hideFilters: function () {
            var body = jQuery('.filters .accordin-filters .nav-tabs'),
                filters = jQuery('.filters'),
                filtersContent = jQuery('.filters .filter-content');
            body.on('click', ' li.active a', function (e) {
                e.preventDefault();
                Event.stop(e);
                var clickedItem = jQuery(this).parent(),
                    tabPanes = jQuery('.tab-content>.tab-pane');
                clickedItem.removeClass('active');
                tabPanes.removeClass('in');
                setTimeout(function () {
                    tabPanes.removeClass('active');
                    filters.css('min-height', filtersContent.outerHeight());
                }, 300);
            });

        },
        homepromotedTabsInit: function () {
            var homepromotedTabs = jQuery('.homepromoted-tabs .owl-carousel');
            homepromotedTabs.owlCarousel({
                loop: true,
                center: false,
                mouseDrag: true,
                touchDrag: true,
                nav: true,
                margin: 25,
                navText: ['<span class="nav-control-prev"></span>', '<span class="nav-control-next"></span>'],
                responsive: {
                    320: {
                        items: 1
                    },
                    480: {
                        items: 2
                    },
                    768: {
                        items: 3
                    },
                    991: {
                        items: 4
                    }
                }
            });
        },
        brandsCarouselInit: function () {
            var brandsOwl = jQuery('#brandsCarousel');
            brandsOwl.owlCarousel({
                loop: true,
                center: false,
                mouseDrag: true,
                touchDrag: true,
                margin: 15,
                autoplay: true,
                autoplayTimeout: 1000,
                autoplaySpeed: 1500,
                autoplayHoverPause: true,
                nav: true,
                navText: ['<span class="nav-control-prev"></span>', '<span class="nav-control-next"></span>'],
                responsive: {
                    320: {
                        items: 2
                    },
                    535: {
                        items: 3
                    },
                    768: {
                        items: 4
                    },
                    992: {
                        items: 5
                    },
                    1200: {
                        items: 6
                    },
                    1400: {
                        items: 6
                    }
                }
            });
            jQuery('#left-slider').css({
                'visibility': 'visible',
                'height': 'auto'
            });
        },
        upsellProducts: function () {

            var MAX_LEFT_POSITION = 175,
                MAX_VISIBLE_WIDTH = 480,
                LEFT_PADDING = 255,
                GUTTER = 20,
                ELEMENT_PADDING = 8;

            var upsellOwl = jQuery('#upsell-carousel'),
                upsellContent = jQuery('.upsell-content');

            upsellOwl.owlCarousel({
                loop: true,
                margin: 20,
                nav: true,
                navText: ['<span class="nav-control-prev"></span>', '<span class="nav-control-next"></span>'],
                responsiveClass: true,
                mouseDrag: true,
                touchDrag: true,
                responsive: {
                    300: {
                        items: 2
                    },
                    360: {
                        items: 3
                    },
                    440: {
                        items: 4
                    },
                    550: {
                        items: 5
                    },
                    600: {
                        items: 6
                    },
                    700: {
                        items: 7
                    },
                    880: {
                        items: 9
                    },
                    992: {
                        items: 7
                    },
                    1200: {
                        items: 9
                    }
                }
            });

            upsellOwl.on('mouseover', '.owl-item', function () {
                if (jQuery(window).width() > MAX_VISIBLE_WIDTH) {

                    var element = jQuery(this),
                        activeItems = jQuery('.owl-item.active'),
                        index = activeItems.index(element.closest('.owl-item.active')) + 1,
                        left = (((activeItems.width() + GUTTER) * index) - LEFT_PADDING),
                        productHoverId = element.children('.upsell-box').data('product-id'),
                        productHover = jQuery(productHoverId),
                        elementHeight = element.outerHeight() - ELEMENT_PADDING;

                    hideAll();
                    productHover.removeClass('hide').addClass('show');

                    if (element.offset().left < MAX_LEFT_POSITION) {
                        left += MAX_LEFT_POSITION;
                        productHover.addClass('left');
                    }
                    productHover.css({top: elementHeight, left: left});
                }
            });

            jQuery(".upsell-products .upsell").on('mouseleave', function () {
                hideAll();
            });

            function hideAll() {
                upsellContent.removeClass('show').removeClass('left').addClass('hide');
            }
        },

        brandsRotatorCarouselInit: function () {
            var brandsRotatorCarousel = jQuery('#brandsRotatorCarousel');
            brandsRotatorCarousel.owlCarousel({
                loop: true,
                center: false,
                autoplay: true,
                autoplayTimeout: 1000,
                autoplaySpeed: 1500,
                autoplayHoverPause: true,
                mouseDrag: true,
                touchDrag: true,
                margin: 15,
                navText: ['<span class="nav-control-prev"></span>', '<span class="nav-control-next"></span>'],
                responsive: {
                    320: {
                        items: 2
                    },
                    535: {
                        items: 3
                    },
                    768: {
                        items: 4
                    },
                    992: {
                        items: 5
                    },
                    1200: {
                        items: 6
                    },
                    1400: {
                        items: 6
                    }
                }
            });


            var body = jQuery('body');

            body.on('click', '.brands-carousel .item a', function () {
                var target = jQuery(this).attr("href"),
                    allTabItems = jQuery('.brandsRotatorCarousel .productTile--product'),
                    allTabPaneItems = jQuery('.brands-rotator .tab-pane'),
                    tabPaneItem = jQuery(target),
                    tabItems = jQuery('.brands-carousel ' + target.replace('#', '.'));

                allTabItems.removeClass('active');
                allTabPaneItems.removeClass('in');
                allTabPaneItems.removeClass('active');
                tabItems.addClass('active');
                tabPaneItem.addClass('active');
                setTimeout(function () {
                    tabPaneItem.addClass('in');
                }, 100);
            });
        },
        fromCategoryCarouselInit: function () {
            var formCategoryCarousel = jQuery('#from-category-carousel');
            formCategoryCarousel.owlCarousel({
                loop: true,
                center: false,
                mouseDrag: true,
                touchDrag: true,
                margin: 25,
                navText: ['<span class="nav-control-prev"></span>', '<span class="nav-control-next"></span>'],
                responsive: {
                    320: {
                        items: 1
                    },
                    480: {
                        items: 2
                    },
                    768: {
                        items: 3
                    },
                    991: {
                        items: 4
                    }
                }
            });
        },
        pagerClick: function () {
            var mini = jQuery('.mini-pager-form').eq(0);

            mini.on('submit', function () {
                var url = mini.attr('action'),
                    current = parseInt(mini.find('.current-page').val()),
                    last = parseInt(mini.find('.last-page').text()),
                    split = url.split('/');
                if (current > 0 && current <= last) {
                    split.pop();
                    split.push(current + '.html');
                    window.setLocation(split.join('/'));
                }
                return false;
            });
        },
        reviewField: function () {
            jQuery('#review_field').keyup(function () {
                var MAX_CHARS = 800;
                var lenght = jQuery(this).val().length,
                    charNumber = jQuery('#char-num');
                if (lenght < MAX_CHARS)
                    charNumber.removeClass('error');
                else
                    charNumber.addClass('error');
                charNumber.text(lenght + '/' + MAX_CHARS);
            });
        },
        /**
         * Show/hide marketing agreement
         */
        showHideAgreement: function () {

            var
                body = jQuery('body'),
                manageNewsletter = jQuery('#manage-newsletter'),
                agreementBox = manageNewsletter.find('.full-agreement-box'),
                acceptanceLabel = manageNewsletter.find('label[for="acceptance"]'),
                acceptanceInput = manageNewsletter.find('#acceptance'),
                checkAcceptanceLabel = manageNewsletter.find('label[for="check-acceptance"]'),
                checkAcceptanceInput = manageNewsletter.find('#check-acceptance');

            body.on('click', '#manage-newsletter .info-open', function () {
                agreementBox.fadeIn();
            });

            body.on('click', '.brands-rotator .brands-carousel.nav-tabs .item a', function () {
                var allItems = jQuery('.brands-rotator .brands-carousel.nav-tabs .item'),
                    clickedItem = jQuery(this).parent();
                allItems.removeClass('active');
                clickedItem.addClass('active');
            });

            body.on('click', '#manage-newsletter .full-agreement-box-close', function () {
                agreementBox.fadeOut();
            });

            acceptanceLabel.on('click', function () {
                checkAcceptanceInput.prop('checked', !checkAcceptanceInput.prop('checked'));
            });

            checkAcceptanceLabel.on('click', function () {
                acceptanceInput.prop('checked', !acceptanceInput.prop('checked'));
                if (acceptanceInput.prop('checked')) {
                    agreementBox.delay(500).fadeOut();
                }
            });
        },
        createFormMaskOnCheckoutPage: function () {
            jQuery('[name="billing[telephone]"]').mask("(+48) 000-000-000", {placeholder: '+48 ___ ___ ___'});
            jQuery('[name="billing[postcode]"]').mask('00-000', {placeholder: '__-___'});
            jQuery('[name="shipping[postcode]"]').mask('00-000', {placeholder: '__-___'});
            jQuery('[name="shipping[telephone]"]').mask('(+48) 000-000-000', {placeholder: '+48 ___ ___ ___'});
        },
        createFormMaskOnCustomerAccount: function () {
            jQuery('[name="telephone"]').mask("(+48) 000-000-000", {placeholder: '+48 ___ ___ ___'});
            jQuery('[name="postcode').mask('00-000', {placeholder: '__-___'});
        },
        searchInputAutoFocus: function () {
            jQuery('#search-button').on('click', function () {
                setTimeout(function () {
                    jQuery('.header-bottom #search-container .UI-SEARCH').focus();
                }, 200);
            });
        },
        shake: function (element) {
            var interval = 125, distance = 10, times = 4;
            jQuery(element).css('position', 'relative');
            for (var i = 0; i < (times + 1); i++) {
                jQuery(element).animate({
                    right: ((i % 2 === 0 ? distance : distance * -1))
                }, interval);
            }
            jQuery(element).animate({right: 0}, interval);
        },
        navTabsMenuTimeout: function () {
            var _ = this, slow, active;
            jQuery('#nav > .parent').mouseenter(function () {
                active = jQuery(this);
                slow = setTimeout(function () {
                    active.addClass('active')
                        .children(".content").fadeIn();
                    _.setContentHeight(active);
                }, 350);
            }).mouseleave(function () {
                clearTimeout(slow);
                active.removeClass('active')
                    .children(".content").fadeOut();
            });
        },
        setContentHeight: function(activeList){
            var content = activeList.find('.content'),
                list = content.children('ul.level0'),
                listHeight = list.height();

            if (listHeight > parseFloat(content.css('min-height'))){
                content.css('height', listHeight);
            } else {
                list.css('height', '100%');
            }

        },
        hideSortByPositionAndRelevance: function () {
            jQuery("#sort-select option[data-type='position']").remove();
            jQuery("#sort-select option[data-type='relevance']").remove();
            jQuery("#sort-select").selectpicker();
        },
        enabledNewsletterSubscribe: function (cfgNumberOfClicks, cfgCookieLifetime) {
            cappingPopup.init(cfgNumberOfClicks, cfgCookieLifetime);
        },
        initAgreementsPopup: function () {
            var agreementsPopup = jQuery('#agreements-popup').modal({
                backdrop: 'static',
                keyboard: false
            });
            agreementsPopup.modal('show');
        },
        declineAgreementsPopup: function() {
            window.location.href = '/agreements/index/declineAgreementsPopup';
        },
        rwdImageMaps: function (imageClass) {
            var _imageClass = imageClass ? imageClass : 'img[usemap]';
            if (jQuery(_imageClass).length) {
                jQuery(_imageClass).rwdImageMaps();
            }
        },
        smoothScroll: function (elementId, speed, correctOffset) {
            if (jQuery(elementId).length) {
                var _correctOffset = correctOffset ? correctOffset : 0;

                jQuery('html, body').animate({
                    scrollTop: jQuery(elementId).offset().top + _correctOffset
                }, speed ? speed : 2000);
            }
        },
        initCustomScroll: function (elementClass) {
            var element = elementClass ? jQuery(elementClass) : jQuery('.custom-scrollbar');
            if (element.length) {
                element.perfectScrollbar();
            }
        },
        removeCustomTemplateScripts: function () {
            jQuery('script[data-mage-custom-script]').remove();
        }
    };

    jQuery(document).ready(function () {
        kontigo.init();
    });

})(jQuery, window, document);

(function () {
    var app = angular.module('fwcDirectives', ['fwcControllers', 'fwcServices']);

    app.directive('matchcosmeticsScrollToProduct', ['matchmedia', function (matchmedia) {
        return function (scope, element) {

            this.scrollToElement = function (top, hash) {
                jQuery('body').scrollTo(hash, {
                    duration: 500,
                    axis: 'y',
                    offset: {top: top, left: 0}
                });
            };

            this.clickingCallback = function () {
                var top = 0,
                    hash = angular.element(element).data('scroll');

                if (matchmedia.is('(min-width: 991px)')) {
                    top = -60;
                    scrollToElement(top, hash);
                }
                if (matchmedia.is('(min-width: 768px)')) {
                    scrollToElement(top, hash);
                }
            };
            element.bind('click', clickingCallback);
        };
    }]);

    app.directive('matchCosmeticsToggle', function ($timeout) {
        return function (scope, element) {

            var setToggle = function (item, delay) {
                var _item = item;
                $timeout(function () {
                    angular.element(_item).toggleClass('in');
                }, delay);
            };

            var clickingCallback = function () {
                var clickedButton = angular.element(element),
                    clickedButtonText = angular.element(element).children(),
                    itemList = angular.element(element).parent().parent().find('.product-box.hidden-box'),
                    delay = 0;

                clickedButton.toggleClass('active');
                clickedButtonText.toggleClass('active');
                if (!clickedButton.hasClass('active')) {
                    angular.element(itemList.get().reverse()).each(function () {
                        setToggle(this, delay);
                        delay += 100;
                    });
                } else {
                    itemList.each(function () {
                        setToggle(this, delay);
                        delay += 50;
                    });
                }
            };

            element.bind('click', clickingCallback);
        };
    });

    app.directive('bundleFixedImages', ['matchmedia', '$window', function (matchmedia, $window) {
        return {
            restrict: 'A',
            link: function ($scope, element) {

                const gutter = 110;

                var ae = angular.element,
                    window = ae($window),
                    headerHeight = ae('header').outerHeight(),
                    _element = ae(element),
                    productDetails = ae('.product-view .product-essentials');

                this.isFixed = function () {
                    if (matchmedia.is('(min-width: 992px)') && (_element.outerHeight() < productDetails.outerHeight())) {
                        ;
                        _element.css({'max-width': _element.outerWidth()});
                        headerHeight = ae('header').outerHeight();
                        self.scrollCallback();
                    } else {
                        _element.removeClass('bottom');
                        _element.removeClass('fixed');
                        _element.css({'max-width': 'initial'});
                    }
                };

                this.scrollCallback = function () {
                    var scrollY = $window.scrollY || $window.pageYOffset || document.documentElement.scrollTop,
                        bottomElementHeight = scrollY + _element.outerHeight() + gutter,
                        bottomElement = ae('.product-view .product-overview').offset().top;

                    if (scrollY > headerHeight) {
                        if (bottomElementHeight > bottomElement) {
                            _element.addClass('bottom');
                        } else {
                            _element.removeClass('bottom');
                            if (!_element.hasClass('fixes')) {
                                _element.addClass('fixed');
                            }
                        }
                    } else {
                        _element.removeClass('bottom');
                        _element.removeClass('fixed');
                    }
                };

                window.bind('scroll', function () {
                    self.isFixed();
                });

                window.bind('resize', function () {
                    _element.css({'max-width': 'initial'});
                    self.isFixed();
                });
            }
        };
    }]);

    app.directive('bundleSelectProduct', function () {

        const CLASS_SELECTOR = '.',
            ID_SELECTOR = '#',
            FADE_IN_TIME = 150,
            FADE_OUT_TIME = 0;

        return function (scope, element) {
            var clickingCallback = function () {
                var _element = angular.element(element),
                    products = angular.element(CLASS_SELECTOR + _element.data('option')),
                    activeProduct = angular.element(CLASS_SELECTOR + _element.data('product')),
                    option = angular.element('.product-bundle .options .option'),
                    bundleOption = option.find(ID_SELECTOR + _element.data('option-id')),
                    bundlePriceElement = angular.element('.product-bundle .product-section-top .bundle-price'),
                    bundlePriceContent = bundlePriceElement.find('.items-price .price .value'),
                    bundlePrice = bundlePriceElement.find('.price-message'),
                    optionPrice = 0;

                bundleOption.val(_element.data('selection-id'));
                products.removeClass('first');
                products.removeClass('active');
                activeProduct.addClass('active');
                products.fadeOut(FADE_OUT_TIME);
                activeProduct.fadeIn(FADE_IN_TIME);

                option.find('.product-selected.active').each(function (index, product) {
                    optionPrice += parseFloat(angular.element(product).data('price'));
                });

                bundlePriceContent.html(parseFloat(optionPrice).toFixed(2).toString().replace(".", ","));

                var priceDiff = parseFloat(optionPrice).toFixed(2) - parseFloat(bundlePrice.data('bundle-price')).toFixed(2);

                bundlePrice.find('span').html(parseFloat(priceDiff).toFixed(2).toString().replace(".", ","));

            };


            element.bind('click', clickingCallback);
        };
    });

    app.directive('bundleShowMoreProducts', function ($window) {

        const GUTTER = 10,
            FADE_IN_TIME = 1000,
            FADE_OUT_TIME = 300;

        var optionsInRow = 0,
            option = angular.element('.product-bundle .options .option'),
            productList = option.find('.products'),
            spans = option.find('.show-more'),
            window = angular.element($window);

        return function (scope, element) {

            var calculateOptionsInRow = function () {
                var options = angular.element('.product-bundle .options .option');
                optionsInRow = 0;
                options.each(function () {
                    if ((angular.element(this).prev().length > 0) && (angular.element(this).position().top !== angular.element(this).prev().position().top)) {
                        return false;
                    }
                    optionsInRow++;
                });

            };

            var toggleClass = function (_element, spans, activeProductList, firstElement, secondElement, thirdElement, optionsInRow, firstElementSecondRow, col) {

                var dynamicElementHeight = activeProductList.height() + GUTTER;

                if (!_element.hasClass('active')) {
                    activeProductList.addClass('open');
                    activeProductList.fadeIn(FADE_IN_TIME);
                    if (optionsInRow !== 1) {
                        spans.removeClass('active');
                        _element.addClass('active');
                        if (col !== 3) {
                            firstElement.css('margin-bottom', dynamicElementHeight);
                            secondElement.css('margin-bottom', dynamicElementHeight);
                            if (optionsInRow !== 2) {
                                thirdElement.css('margin-bottom', dynamicElementHeight);
                            }
                        } else {
                            thirdElement.css('margin-bottom', dynamicElementHeight);
                            if (optionsInRow === 2) {
                                firstElementSecondRow.css('margin-bottom', dynamicElementHeight);
                            }
                        }
                    } else {
                        _element.addClass('active');
                        _element.css('margin-bottom', dynamicElementHeight);
                    }
                } else {
                    activeProductList.removeClass('open');
                    activeProductList.fadeOut(FADE_OUT_TIME);
                    if (optionsInRow !== 1) {
                        _element.removeClass('active');
                        if (col !== 3) {
                            firstElement.css('margin-bottom', 0);
                            secondElement.css('margin-bottom', 0);
                            if (optionsInRow !== 2) {
                                thirdElement.css('margin-bottom', 0);
                            }
                        } else {
                            thirdElement.css('margin-bottom', 0);
                            if (optionsInRow === 2) {
                                firstElementSecondRow.css('margin-bottom', 0);
                            }
                        }
                    } else {
                        _element.removeClass('active');
                        _element.css('margin-bottom', 0);
                    }
                }
            };

            var clickingCallback = function () {

                var _element = angular.element(element),
                    row = parseInt(_element.data('row')),
                    col = parseInt(_element.data('col')),
                    firstElement = angular.element('.col-1-row-' + row),
                    firstElementSecondRow = angular.element('.col-1-row-' + (row + 1)),
                    secondElement = angular.element('.col-2-row-' + row),
                    thirdElement = angular.element('.col-3-row-' + row),
                    activeProductList = jQuery('.' + _element.data('active-element'));


                calculateOptionsInRow();

                productList.removeClass('open');
                productList.fadeOut(FADE_OUT_TIME);
                spans.css('margin-bottom', 0);
                toggleClass(_element, spans, activeProductList, firstElement, secondElement, thirdElement, optionsInRow, firstElementSecondRow, col);

            };


            var resizeCallback = function () {
                productList.removeClass('open');
                spans.css('margin-bottom', 0);
                productList.fadeOut(FADE_OUT_TIME);
                spans.removeClass('active');
            };

            option.on("click", ".products .close-products", function () {
                productList.removeClass('open');
                spans.css('margin-bottom', 0);
                productList.fadeOut(FADE_OUT_TIME);
                spans.removeClass('active');
            });


            element.bind('click', clickingCallback);
            window.bind('resize', resizeCallback);

        };
    });

    app.directive('wishlistAddAllToCart', function () {
        return function (scope, element) {

            var calculateQty = function (qtyItems) {

                var qtysArray = [];

                qtyItems.each(function (index, input) {
                    var idxStr = input.name,
                        idx = idxStr.replace(/[^\d.]/g, '');
                    if (input.value) {
                        qtysArray[idx] = input.value;
                    }

                });
                return qtysArray;
            };


            var clickingCallback = function () {

                var form = angular.element('#wishlist-add-all-to-cart-form'),
                    qtyItems = form.find('.qty input');

                form.find('#qty').val(JSON.stringify(calculateQty(qtyItems)));
                form.submit();

            };
            element.bind('click', clickingCallback);
        };
    });

    app.directive('matchCosmeticsAvatar', ['matchCosmeticsService', function (matchCosmeticsService) {
        return {
            link: function (scope, element) {
                var _element = jQuery(element),
                    matchCosmeticsAvatar = jQuery('.match-cosmetics-avatar');

                matchCosmeticsService.saveMatchCosmeticsAvatarToStorage(_element);
                matchCosmeticsService.getMatchCosmeticsAvatarFromStorage(_element);

                var hideAvatar = function () {
                    matchCosmeticsAvatar.addClass('hide');
                    matchCosmeticsService.saveHiddenAvatarToStorage();
                };

                jQuery(window).load(function () {
                    if (matchCosmeticsService.checkIfAvatarIsHidden()) {
                        hideAvatar();
                    } else {
                        matchCosmeticsService.saveMatchCosmeticsFaceMapToStorage();
                        matchCosmeticsAvatar.addClass('fade');
                    }
                });


                matchCosmeticsAvatar.on("click", ".close-action", function () {
                    hideAvatar();
                });
            }
        };
    }]);

    app.directive('matchCosmeticsValidate', function () {
        return {
            controller: ['$scope', '$element', 'matchCosmeticsService', function ($scope, element, matchCosmeticsService) {

                var matchCosmetics = jQuery('.match-cosmetics'),
                    radioList = matchCosmetics.find('.face-option'),
                    validationAdvice = matchCosmetics.find('.validation-advice'),
                    isValid = true;

                var checkValidation = function () {
                    validationAdvice.find('ul').html('');
                    radioList.each(function () {
                        var radioInput = jQuery(this),
                            name = radioInput.data('id'),
                            title = radioInput.data('title');

                        if (!radioList.find("input:radio[name=" + name + "]").is(":checked")) {
                            isValid = false;
                            validationAdvice.find('ul').append('<li>' + title + '</li>');
                        }
                    });

                };

                var hideFaceOptions = function () {
                    matchCosmetics.find('.face-options').hide();
                    matchCosmetics.find('.face-option').hide();
                };

                var isValidCallback = function () {

                    isValid = true;

                    matchCosmeticsService.getMatchCosmeticsFaceMapFromStorage();
                    checkValidation();

                    if (isValid) {
                        validationAdvice.hide();
                        hideFaceOptions();
                        matchCosmeticsService.submitMatchCosmeticsFrom();
                    } else {
                        validationAdvice.show();
                    }
                };

                matchCosmetics.on("click", ".navigation button", function () {
                    isValidCallback();
                });

            }]
        };
    });

    app.directive('matchCosmetics', function () {
        return {
            controller: ['$scope', '$element', '$window', 'matchCosmeticsService', function ($scope, element, $window, matchCosmeticsService) {

                const CLASS_SELECTOR = '.';

                var matchCosmetics = jQuery('.match-cosmetics'),
                    window = jQuery($window);

                var updateSvgFileCallback = function () {
                    matchCosmeticsService.updateMatchCosmeticsFaceMapToStorage();
                    matchCosmeticsService.getMatchCosmeticsFaceMapFromStorage();
                };

                var hideFaceOptions = function () {
                    matchCosmetics.find('.face-options').hide();
                    matchCosmetics.find('.face-option').hide();
                };

                var hideHoveredElements = function () {
                    var svg = matchCosmetics.find('.svg-container svg');
                    svg.find('g').removeClass('show');
                };

                var showFaceOptions = function (event, _this) {
                    if (jQuery(_this).attr('class')) {
                        var clickedElement = CLASS_SELECTOR + jQuery(_this).attr('class').replace(/^(\S*).*/, '$1'),
                            offset = matchCosmetics.find('#match-cosmetics-face').offset(),
                            options = matchCosmetics.find('.face-options'),
                            posX = event.pageX - offset.left,
                            posY = event.pageY - offset.top,
                            gutter = 15;

                        hideFaceOptions();

                        if (options.find(clickedElement).length) {
                            options.find(clickedElement).show();
                            options.show();
                            options.css({top: posY + gutter, left: posX + gutter});
                        }
                    } else {
                        hideFaceOptions();
                    }
                };

                window.on("resize", function () {

                    hideFaceOptions();

                });

                matchCosmetics.on("click", function (event) {
                    var clickedElement = jQuery(event.target);
                    if (clickedElement.parent().is('g')) {
                        showFaceOptions(event, clickedElement.parent());
                    } else if (clickedElement.is('g')) {
                        showFaceOptions(event, clickedElement);
                    } else {
                        if (!clickedElement.is('label') && (!clickedElement.hasClass('controls'))) {
                            hideFaceOptions();
                        }
                    }
                });


                matchCosmetics.on("mouseover", "#match-cosmetics-face .svg-container svg g", function () {
                    var svg = matchCosmetics.find('.svg-container svg'),
                        _elementClass = jQuery(this).attr('class').split(' ')[0],
                        hoverElement = svg.find(CLASS_SELECTOR + _elementClass + CLASS_SELECTOR + 'hover');

                    svg.find('g').removeClass('show');
                    hoverElement.addClass('show');
                });

                matchCosmetics.on("mouseover", ".face-options", function () {
                    hideHoveredElements();
                });

                matchCosmetics.on("mouseleave", ".svg-container", function () {
                    hideHoveredElements();
                });

                matchCosmetics.on("click", ".face-options .close-ico", function () {
                    hideFaceOptions();
                });

                matchCosmetics.on("click", "#match-cosmetics-face .face-options .face-option .content .controls input", function (event) {
                    event.stopPropagation();
                    updateSvgFileCallback();
                });

            }]
        };
    });

    app.directive('svgImage', ['$http', 'matchCosmeticsService', function ($http, matchCosmeticsService) {
        return {
            restrict: 'E',
            link: function (scope, element) {
                var imgURL = element.attr('src'),
                    matchCosmetics = jQuery('.match-cosmetics'),
                    request = $http.get(
                        imgURL,
                        {'Content-Type': 'application/xml'}
                    );

                scope.manipulateImgNode = function (data, elem) {
                    var $svg = jQuery(data)[4],
                        imgClass = elem.attr('class');

                    if (typeof(imgClass) !== 'undefined') {
                        var classes = imgClass.split(' ');
                        for (var i = 0; i < classes.length; ++i) {
                            $svg.classList.add(classes[i]);
                        }
                    }
                    $svg.removeAttribute('xmlns:a');
                    return $svg;
                };

                request.success(function (data) {
                    element.replaceWith(scope.manipulateImgNode(data, element));
                    matchCosmeticsService.saveMatchCosmeticsFaceMapToStorage();
                    matchCosmeticsService.getMatchCosmeticsFaceMapFromStorage();
                    matchCosmetics.find('#match-cosmetics-face').addClass('fade');
                    matchCosmetics.find('.loader').removeClass('active');
                    matchCosmetics.find('.reset-control').addClass('active');
                });
            }
        };
    }]);

    app.directive('mouseoverCollision', function ($window) {
        return function (scope, element) {
            var window = angular.element($window),
                content = angular.element(element).children('.content');
            const MAX_LEFT_POSITION = 175;

            var mouseoverCallback = function () {
                if (angular.element(element).offset().left < MAX_LEFT_POSITION) {
                    content.addClass('left');
                }
                else {
                    content.removeClass('left');
                }
            };
            element.bind('mouseover', mouseoverCallback);
            window.bind('resize', mouseoverCallback);
        };
    });

    app.directive('qtyNavigation', function () {
        return function (scope, element) {
            var clickingCallback = function () {

                const MIN_VALUE = 1,
                    MAX_VALUE = 99;

                var qty = angular.element('#qty'),
                    clickedElement = angular.element(element);

                if (clickedElement.hasClass('less') && parseInt(qty.val()) > MIN_VALUE) {
                    qty.val(parseInt(qty.val()) - 1);
                } else if (clickedElement.hasClass('more') && parseInt(qty.val()) < MAX_VALUE) {
                    qty.val(parseInt(qty.val()) + 1);
                }
            };
            element.bind('click', clickingCallback);
        };
    });

    app.directive('cartQtyNavigation', function () {
        return function (scope, element) {
            var clickingCallback = function () {

                var qty = angular.element(element).parent().children('input'),
                    clickedElement = angular.element(element),
                    updateCart = angular.element('#update-cart');

                const MIN_VALUE = 1,
                    MAX_VALUE = 99;

                if (clickedElement.hasClass('less') && parseInt(qty.val()) > MIN_VALUE) {
                    qty.val(parseInt(qty.val()) - 1);
                    updateCart.click();
                } else if (clickedElement.hasClass('more') && parseInt(qty.val()) < MAX_VALUE) {
                    qty.val(parseInt(qty.val()) + 1);
                    updateCart.click();
                }
            };
            element.bind('click', clickingCallback);
        };
    });

    app.directive('setRatings', function () {
        return function (scope, element) {

            var elementParent = (angular.element(element).parent()),
                elementRatingDiv = elementParent.children('.rating');

            var calcRate = function ($rate) {
                return (($rate + 1) / 5) * 100;
            };

            var callback = function () {
                var elementRate = calcRate(angular.element(element).data('rate'));
                elementRatingDiv.width(elementRate);
            };

            var mouseleaveCallback = function () {
                var checkedRate = calcRate(elementParent.children("input[type='radio']:checked").data('rate'));
                if (isNaN(checkedRate)) {
                    elementRatingDiv.width(0);
                } else {
                    elementRatingDiv.width(checkedRate);
                }
            };

            element.bind('click mouseover', callback);
            element.bind('mouseleave', mouseleaveCallback);
        };
    });

    app.directive('scrollToReview', function ($timeout) {
        return function (scope, element) {

            var scrollTo = function (hash) {
                var tab = jQuery('#headingFour a');

                if (tab.hasClass('collapsed')) {
                    tab.trigger('click');
                }
                $timeout(function () {
                    jQuery('body').scrollTo(hash, {
                        duration: 500,
                        axis: 'y',
                        offset: {top: -125, left: 0}
                    });
                }, 400);
            };

            var clickingCallback = function () {
                var hash = angular.element(element).data('scroll');
                scrollTo(hash);
            };

            element.bind('click', clickingCallback);
        };
    });

    app.directive('relatedToggle', function ($timeout) {
        return function (scope, element) {


            var setToggle = function (item, delay) {
                var _item = item;
                $timeout(function () {
                    angular.element(_item).toggleClass('in');
                }, delay);
            };

            var clickingCallback = function () {

                var clickedButton = angular.element(element),
                    clickedButtonText = angular.element(element).children(),
                    itemList = angular.element('.related-product .related-box.hidden-box'),
                    delay = 0;

                clickedButton.toggleClass('active');
                clickedButtonText.toggleClass('active');
                if (!clickedButton.hasClass('active')) {
                    angular.element(itemList.get().reverse()).each(function () {
                        setToggle(this, delay);
                        delay += 50;
                    });
                } else {
                    itemList.each(function () {
                        setToggle(this, delay);
                        delay += 50;
                    });
                }
            };
            element.bind('click', clickingCallback);
        };
    });

    app.directive('addToCart', function () {
        return {
            controller: ['$scope', '$element', 'cartService', function ($scope, element, cartService) {

                element.on('click', function (e) {
                    e.preventDefault();
                    var url = element.data('url');
                    if (url) {
                        cartService.cartCallback(url, null);
                    } else {
                        var form = angular.element('#product_addtocart_form');
                        cartService.cartCallback(form.attr('action'), form.serialize());
                    }
                });

            }]
        };
    });

    app.directive('fixedFilters', ['matchmedia', '$window', function (matchmedia, $window) {
        return {
            restrict: 'A',
            link: function () {
                var ae = angular.element,
                    window = ae($window),
                    filtersContent = ae('.filters .filter-content'),
                    filters = ae('.filters'),
                    additionalFilters = ae('.filters .additional-filters'),
                    gutter = ae('#header .header-container .menu nav #nav').outerHeight(),
                    self = this;

                this.isFixed = function () {
                    if (matchmedia.is('(min-width: 992px)')) {
                        var topPosition = filters.offset().top;
                        self.scrollCallback(topPosition);
                    } else {
                        filtersContent.removeClass('fixed');
                        additionalFilters.removeClass('fixed-additional');
                    }
                };

                this.scrollCallback = function (topPosition) {
                    var scrollY = $window.scrollY || $window.pageYOffset || document.documentElement.scrollTop;
                    if (scrollY > topPosition - gutter) {
                        if (!filtersContent.hasClass('fixed')) {
                            filters.css('min-height', filtersContent.outerHeight());
                            filtersContent.addClass('fixed');
                            additionalFilters.addClass('fixed-additional');
                        }
                    } else {
                        filtersContent.removeClass('fixed');
                        additionalFilters.removeClass('fixed-additional');
                        filters.css('min-height', filtersContent.outerHeight());
                    }
                };

                window.bind('scroll resize', function () {
                    self.isFixed();
                });
            }
        };
    }]);

    app.directive('addToWishlist', function () {
        return {
            controller: ['$scope', '$element', '$compile', 'addToWishlistService', function ($scope, element, $compile, addToWishlistService) {
                element.on('click', function (e) {
                    e.preventDefault();
                    var _element = angular.element(element),
                        isLogged = angular.element('#logged-user').val(),
                        url = _element.data('url');

                    if ((isLogged === 'true') && (!element.hasClass('active'))) {
                        addToWishlistService.addToWishlist(url, _element);
                    } else {
                        addToWishlistService.redirectToWishlist(url);
                    }

                });
            }]
        };
    });

    app.directive('checkWishlist', function () {
        return {
            controller: ['$scope', '$element', '$compile', 'addToWishlistService', function ($scope, element, $compile, addToWishlistService) {
                var _element = angular.element(element),
                    isLogged = angular.element('#logged-user').val(),
                    url = _element.data('url');
                if (isLogged === 'true') {
                    addToWishlistService.setActiveWishlistItems(url);
                }
            }]
        };
    });

    app.directive('fixedHeader', function ($window, $timeout, matchmedia) {
        return function (scope, element) {
            var window = angular.element($window),
                $j = jQuery,
                header = $j('#header'),
                gutter = 20,
                fixedHeader = $j(element),
                searchContent = $j('#header #search-container');
            var scrollCallback = function () {
                if (matchmedia.is('(min-width: 992px)')) {

                    var scrollY = $window.scrollY || $window.pageYOffset || document.documentElement.scrollTop;

                    if ((scrollY > header.outerHeight() + gutter) && (!fixedHeader.hasClass('fixed'))) {
                        $timeout(function () {
                            fixedHeader.removeClass('in');
                            fixedHeader.addClass('fixed');
                            fixedHeader.collapse('show');
                        }, 100);
                    }
                    if ((scrollY < header.outerHeight() + gutter)) {
                        searchContent.collapse('hide');
                        if (fixedHeader.hasClass('fixed')) {
                            fixedHeader.removeClass('fixed');
                            fixedHeader.addClass('in');
                        }
                    }
                } else {
                    fixedHeader.removeClass('fixed');
                }
            };
            window.bind('scroll resize', scrollCallback);
        };
    });

    app.directive('mobileMenu', function ($timeout) {
        return function (scope, element) {
            var clickingCallback = function () {
                var menu = angular.element('#header .header-container nav');
                var overlay = angular.element('#overlay');
                if (menu.hasClass('active')) {
                    overlay.removeClass('active');
                    menu.removeClass('active');
                    $timeout(function () {
                        overlay.addClass('hide');
                    }, 300);
                } else {
                    overlay.removeClass('hide');
                    overlay.addClass('active');
                    menu.addClass('active');
                }
            };
            element.bind('click', clickingCallback);
        };
    });

    app.directive('overlay', function (sidebarService) {
        return {
            restrict: 'E',
            replace: true,
            link: function (scope) {
                scope.data = [];

                scope.isActive = function () {
                    var overlay = angular.element('#sidebar-overlay'),
                        isOpen = sidebarService.isOpen;

                    if (isOpen){
                        overlay.fadeIn(150, 'linear');
                    }
                    else{
                        overlay.fadeOut(150, 'linear');
                    }

                    return isOpen ? 'active' : '';
                };

                scope.deactivate = function () {
                    sidebarService.close();
                };
            },
            template: '<div id="sidebar-overlay" ng-class="isActive()" ng-click="deactivate()"/>'
        };
    });

    app.directive('cartTotal', function () {
        return {
            restrict: 'EA',
            replace: true,
            controller: ['$scope', '$attrs', '$element', 'cartService', function ($scope, $attrs, $element, cartService) {

                $scope.$watch(function () {
                    return cartService.totals;
                }, function (value) {
                    $scope.totals = value;
                });
            }],
            template: '<span class="cart-totals-value">{{totals | currency}}</span>'
        }}
    );

    app.directive('cartShipping', function () {
            return {
                restrict: 'E',
                replace: true,
                controller: ['$scope', '$attrs', '$element', 'cartService', function ($scope, $attrs, $element, cartService) {

                    $scope.$watch(function () {
                        return cartService.shipping + cartService.paymentFees;
                    }, function (value) {
                        $scope.shipping = value;
                    });
                }],
                template: '<div class="cart-info shipping" ng-show="shipping > 0"><span class="cart-info-label" translate="Shipping amount:"></span><span class="cart-info-value">{{shipping | currency}}</span> </div>'
            };
        }
    );

    app.directive('translate', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var text        = attrs.translate,
                    translation = Translator.translate(text);

                element.text(translation);
            }
        };
    });

    app.directive('freeshipping', function () {
            return {
                restrict: 'E',
                replace: true,
                controller: ['$scope', '$attrs', '$element', 'cartService', function ($scope, $attrs, $element, cartService) {
                    var dataFree = angular.element($element).parent().data('free');

                    $scope.$watch(function () {
                        return cartService.totals - cartService.discount - cartService.shipping - cartService.paymentFees;
                    }, function (value) {
                        $scope.freeshipping = dataFree - value;
                    });
                }],
                template: '<span><div ng-if="freeshipping > 0"><span translate="To free delivery miss you"></span> <strong>{{freeshipping | currency}}</strong></div><div ng-if="freeshipping <= 0"><span translate="Congratulations, receive a free delivery"></span></div></span>'
            };
        }
    );

    app.directive('qtyCart', function () {
        return {
            restrict: 'EA',
            replace: true,
            controller: ['$scope', '$attrs', '$element', 'cartService', function ($scope, $attrs, $element, cartService) {

                $scope.$watch(function () {
                    return cartService.qty;
                }, function (value) {
                    $scope.qty = value;
                });
            }],
            template: '<div><span ng-if="qty > 0" class="qty">({{qty}})</span></div>'
        };
        }
    );

    app.directive('cartDiscount',
        function () {
            return {
                restrict: 'E',
                replace: true,
                controller: ['$scope', '$attrs', '$element', 'cartService', function ($scope, $attrs, $element, cartService) {

                    $scope.$watch(function () {
                        return cartService.discount;
                    }, function (value) {
                        $scope.discount = value;
                    });
                }],
                template: '<div class="cart-info discount" ng-if="discount < 0"><span class="cart-info-label" translate="Discount amount:"><</span> <span class="cart-info-value">{{discount | currency}}</span></div>'
            };
        }
    );

    app.directive('leftBannerTimeManagement', function ($filter) {
        return function () {

            var ae            = angular.element,
                dateTimeStamp = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                bannerItems   = ae('#swiper-container .swiper-wrapper .item');

            angular.forEach(bannerItems, function(element) {

                var toRemove  = 0,
                    startTime = ae(element).data('start-time'),
                    endTime   = ae(element).data('end-time'),
                    bannersLeftImages  = ae('#swiper-container .swiper-wrapper .item'),
                    bannersLeftIndicators = ae('#carousel-banners-left .carousel-indicators li');

                if ((startTime !== undefined) && (endTime === undefined) && (dateTimeStamp <= endTime)) {
                    toRemove = 1;
                } else if ((startTime === undefined) && (endTime !== undefined) && (dateTimeStamp <= endTime)) {
                    toRemove = 1;
                } else if ((startTime !== undefined) && (endTime !== undefined) && !(dateTimeStamp >= startTime && dateTimeStamp <= endTime)) {
                    toRemove = 1;
                }
                if (toRemove === 1) {
                    ae(element).remove();
                    ae(bannersLeftIndicators[bannersLeftIndicators.length - 1]).remove();
                    ae(bannersLeftImages).parent().find('.item.active').removeClass('active');
                }
            });

            var carouselItems    = ae('#swiper-container .swiper-wrapper .item'),
                bannerIndicators = ae('#carousel-banners-left .carousel-indicators');

            ae(carouselItems[0]).addClass('active');
            ae(bannerIndicators).removeClass('hidden');
        };
    });

    app.directive('rightBannerTimeManagement', function ($filter) {
        return function () {

            var ae            = angular.element,
                dateTimeStamp = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                bannerItems   = ae('#carousel-banners-right .carousel-inner .item');

            angular.forEach(bannerItems, function(element) {

                var toRemove  = 0,
                    startTime = ae(element).data('start-time'),
                    endTime   = ae(element).data('end-time'),
                    bannersRightImages = ae('#carousel-banners-right .carousel-inner .item'),
                    bannersRightIndicators = ae('#carousel-banners-right .carousel-indicators li');

                if ((startTime !== undefined) && (endTime === undefined) && (dateTimeStamp <= endTime)) {
                    toRemove = 1;
                } else if ((startTime === undefined) && (endTime !== undefined) && (dateTimeStamp <= endTime)) {
                    toRemove = 1;
                } else if ((startTime !== undefined) && (endTime !== undefined) && !(dateTimeStamp >= startTime && dateTimeStamp <= endTime)) {
                    toRemove = 1;
                }
                if (toRemove === 1) {
                    ae(element).remove();
                    ae(bannersRightIndicators[bannersRightIndicators.length - 1]).remove();
                    ae(bannersRightImages).parent().find('.item.active').removeClass('active');
                }
            });

            var carouselItems    = ae('#carousel-banners-right .carousel-inner .item'),
                bannerIndicators = ae('#carousel-banners-right .carousel-indicators');

            ae(carouselItems[0]).addClass('active');
            ae(bannerIndicators).removeClass('hidden');
        };
    });

    app.directive('scrollToTop', function () {
        return function () {

            var ae = angular.element,
                isGoToShown = false,
                anchor = jQuery('#go-to-top');

            ae(window).bind('scroll', function() {

                if ((ae('body').hasClass('catalog-category-view')) ||
                    (ae('body').hasClass('cms-harryj-kreator-makijazu-kontigo-html'))) {
                    var offsetTop   = jQuery(document).scrollTop();

                    if (anchor.length && offsetTop > 50 && isGoToShown === false) {
                        anchor.stop().fadeIn(300);
                        isGoToShown = true;
                    } else if (anchor.length && offsetTop <= 50 && isGoToShown === true) {
                        anchor.stop().fadeOut(300);
                        isGoToShown = false;
                    }
                }
            });

            ae('#go-to-top').bind('click', function() {
                jQuery('html, body').animate({ scrollTop: 0 }, 1000);
                return false;
            });

        };
    });

    app.directive('scrollToAnchors', function () {
        return function () {

            jQuery('.harryj .buttons-nav a').click(function() {
                jQuery('html, body').animate({
                    scrollTop: jQuery(jQuery(this).attr('href')).offset().top - 50
                }, 500);
                return false;
            });
        };
    });

    app.directive('cartProducts', function() {
        return {
            restrict: 'E',
            template: '<a ng-click="remove($event)" href="{{product.delete_url}}" title="{{product.name}}" class="product-remove"></a> ' +
            '<a href="{{product.url}}" title="{{product.name}}" class="product-image"> ' +
            '<img ng-src="{{product.image}}" width="90" height="90" alt="{{product.name}}" /> ' +
            '</a> ' +
            '<div class="product-details"> ' +
            '<a href="{{product.url}}" class="product-name">{{product.name}}</a> ' +
            '<div class="product-price"> ' +
            '<span ng-if="priceExist(product.price.rulePrice,product.price.specialPrice)">' +
                '<span  ng-show="{{product.price.normalPrice}}" class="old-price">{{product.price.normalPrice | currency}}</span>' +
            '</span> ' +
            '<span ng-show="{{product.price.specialPrice}}" class="special-price">{{product.price.specialPrice | currency}} </span>' +
            '<span ng-show="{{product.price.rulePrice}}" class="special-price catalog-rule-price">{{product.price.rulePrice | currency}}</span> ' +
            '<span class="product-qty" ng-show="product.qty > 1">(x{{product.qty}})</span> ' +
            '<span ng-hide="priceExist(product.price.specialPrice,product.price.rulePrice)">' +
                '<span>{{product.price.normalPrice | currency}}</span>'+
            '</span> ' +
            '</div> ' +
            '</div>'
        };
    });

    app.directive('compile', function($compile) {
        return function(scope, element, attrs) {
            scope.$watch(
                function(scope) {
                    return scope.$eval(attrs.compile);
                },
                function(value) {
                    element.html(value);

                    // NOTE: we only compile .childNodes so that
                    // we don't get into infinite loop compiling ourselves!
                    $compile(element.contents())(scope);
                }
            );
        };
    });

})();

(function () {
    var app = angular.module('fwcFilters', []);
})();
(function () {
    var app = angular.module('fwcServices', []);

    app.service('matchCosmeticsService', ['$sessionStorage', function ($sessionStorage) {

        const ID_SELECTOR = '#',
            CLASS_SELECTOR = '.',
            FILL_SELECTOR = 'full';

        var matchCosmetics = angular.element('.match-cosmetics');

        this.createMatchCosmeticsFaceMapJson = function () {
            var radioList = matchCosmetics.find('.face-option'),
                jsonArray = {};

            radioList.each(function () {
                var radioInput = angular.element(this),
                    radioId = radioInput.data('id'),
                    singleRadioInput = radioList.find("input:radio[name=" + radioId + "]:checked");

                if (singleRadioInput.val()) {
                    jsonArray[radioId] = {
                        "id": singleRadioInput.val(),
                        "svg_id": singleRadioInput.data('id'),
                        "class_name": singleRadioInput.data('class-name'),
                        "title": singleRadioInput.data('title'),
                        "attribute_id": singleRadioInput.data('attribute-id')
                    };
                }
            });
            return jsonArray;
        };

        this.resetMatchCosmeticsFaceMapJson = function () {

            var radioList = matchCosmetics.find('.face-option');
            radioList.find("input:radio:checked").prop("checked",false);
        };

        this.updateMatchCosmeticsFaceMapToStorage = function () {
            $sessionStorage.matchCosmeticsJson = JSON.stringify(this.createMatchCosmeticsFaceMapJson());
        };

        this.saveMatchCosmeticsFaceMapToStorage = function () {
            var faceMap = angular.element('#match-cosmetic-map').val();
            if (faceMap) {
                $sessionStorage.matchCosmeticsJson = JSON.parse(JSON.stringify(faceMap));
            }
        };

        this.saveMatchCosmeticsAvatarToStorage = function (_element) {
            var avatarUrl = _element.data('avatar-url');
            if (avatarUrl !== "") {
                $sessionStorage.avatarUrl = avatarUrl;
            }
        };

        this.saveHiddenAvatarToStorage = function () {
            if (!$sessionStorage.hideAvatar) {
                $sessionStorage.hideAvatar = true;
            }
        };

        this.checkIfAvatarIsHidden = function () {
            return $sessionStorage.hideAvatar;
        };

        this.getMatchCosmeticsAvatarFromStorage = function (_element) {
            if ($sessionStorage.avatarUrl) {
                angular.element(_element).attr('src', $sessionStorage.avatarUrl);
            }
        };

        this.getMatchCosmeticsFaceMapFromStorage = function () {
            if ($sessionStorage.matchCosmeticsJson) {
                var jsonArray = JSON.parse($sessionStorage.matchCosmeticsJson);

                if (jsonArray) {
                    jQuery.each(jsonArray, function (key, item) {
                        if (matchCosmetics.find('svg').find(ID_SELECTOR + item.svg_id).length) {
                            matchCosmetics.find('svg').find(CLASS_SELECTOR + item.class_name).attr("style", 'display:none');
                            matchCosmetics.find('svg').find(CLASS_SELECTOR + item.class_name + CLASS_SELECTOR + FILL_SELECTOR).attr("style", 'display:block');
                            matchCosmetics.find('svg').find(ID_SELECTOR + item.svg_id).attr("style", 'display:block');
                        }
                        matchCosmetics.find(".face-options input[name=" + key + "][value=" + item.id + "]").attr('checked', 'checked');
                    });
                }
            }
        };

        this.setResetFaceMapFromStorage = function () {
            if ($sessionStorage.matchCosmeticsJson) {
                var jsonArray = JSON.parse($sessionStorage.matchCosmeticsJson);

                if (jsonArray) {
                    matchCosmetics.find('svg').find('g').attr('style', 'display:none');
                    matchCosmetics.find('svg').find('g.default').attr('style', 'display:block');
                    matchCosmetics.find('svg').find('g.empty').attr('style', 'display:block');
                }

                delete $sessionStorage.matchCosmeticsJson;
            }
        };

        this.createStylesToSvg = function () {
            if ($sessionStorage.matchCosmeticsJson) {
                var jsonArray = JSON.parse($sessionStorage.matchCosmeticsJson),
                    styles = '';
                const ID_SELECTOR = '#',
                    CLASS_SELECTOR = '.';
                if (jsonArray) {
                    jQuery.each(jsonArray, function (key, item) {
                        if (matchCosmetics.find('svg').find(ID_SELECTOR + item.svg_id).length) {
                            styles += CLASS_SELECTOR + item.class_name + '{display:none} ';
                            styles += ID_SELECTOR + item.svg_id + '{display:block} ';
                        }
                    });
                }
                return styles;
            }
            return '';
        };


        this.submitMatchCosmeticsFrom = function () {
            const CLASS_SELECTOR = '.',
                FILL_SELECTOR = 'full',
                EMPTY_SELECTOR = 'empty';

            var mapInput = matchCosmetics.find('input[name="match-cosmetic-map"]'),
                imageInput = matchCosmetics.find('input[name="avatar"]'),
                faceMap = $sessionStorage.matchCosmeticsJson,
                $styles = '';

            matchCosmetics.find('svg').find('g' + CLASS_SELECTOR + FILL_SELECTOR).attr("style", 'display:none');
            matchCosmetics.find('svg').find('g' + CLASS_SELECTOR + EMPTY_SELECTOR).attr("style", 'display:none');
            matchCosmetics.find('.svg-container').addClass('loader');
            matchCosmetics.find('.loader').addClass('active');
            matchCosmetics.find('.loader-text').addClass('active');
            mapInput.val(faceMap);
            $styles = this.createStylesToSvg();
            if ($styles !== '') {
                imageInput.val(btoa(unescape(encodeURIComponent($styles))));
            }
            matchCosmetics.find('form').submit();
        };

    }]);

    app.service('addToWishlistService', ['$window', '$http', function ($window, $http) {

        this.addToWishlist = function (url, _element) {

            const IS_AJAX = '?isAjax=1';

            var _this = this;
            $http({
                url: url + IS_AJAX,
                method: 'GET',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).success(function (message) {
                if (message['status'] === 'success') {
                    angular.element(_element).addClass('active');
                    _this.createToast(Translator.translate('Information'), message['message']);
                } else {
                    _this.createToast(Translator.translate('Warning'), message['message']);
                }
            });
        };


        this.setActiveWishlistItems = function (url) {
            $http({
                url: url,
                method: 'POST',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).success(function (productInWishlistIds) {
                angular.forEach(productInWishlistIds, function (id) {
                    angular.element('*[data-wishlist-product-id="' + id + '"]').addClass('active');
                });
            });
        };


        this.redirectToWishlist = function (url) {
            $window.location.href = url;
        };

        this.createToast = function (heading, text) {
            jQuery.toast({
                heading: heading,
                text: text,
                position: 'top-right',
                stack: 4
            });
        };

    }]);

    app.service('cartService', ['$http', 'sidebarService', '$compile', function ($http, sidebarService) {

        const IS_AJAX = '?isAjax=1';

        var ae = angular.element;
        var cart = {},
            isCheckoutPage = ae('body').hasClass('checkout-cart-index');

        cart.products = {};
        cart.empty = false;
        cart.totals = null;
        cart.qty = ae('.top-links .qty').attr('session');
        cart.freeShippingData = ae('#sidebar-promo-label').data('free');
        cart.discount = null;
        cart.loader = false;
        cart.message = null;
        cart.checkoutCartBlocks = {
            items: null,
            totals_block: null,
            freeshipping: null
        };

        cart.cartCallback = function (url, params) {
            var _this = this;
            $http({
                url: url,
                data: params,
                method: 'POST',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).success(function(response) {

                cart.qty = response['qtyCartSession'];
                cart.totals = response['totals'];
                cart.discount = response['discount'];

                if (response['status'] === 'success') {
                    _this.createToast(Translator.translate('Information'), response['message'], 1);
                } else {
                    _this.createToast(Translator.translate('Warning'), response['message'], 0);
                }
            });
        };

        cart.createToast = function (heading, text, status) {

            if (status) {

                var
                    progressInfo = null,
                    progressValue = null,
                    shippingValue = cart.toFreeShippingValue(),
                    shippingProgress = cart.calculateShippingProgress();

                if (shippingProgress > 100) {
                    progressValue = 100;
                    progressInfo = Translator.translate('Congratulations to get free delivery!');
                } else {
                    progressValue = Math.round(shippingProgress);
                    progressInfo = Translator.translate('Free delivery starts from') + shippingValue + Translator.translate('PLN');
                }

                jQuery.toast({
                    heading: heading,
                    text: cart.prepareToastTemplate(text, progressInfo),
                    position: 'top-right',
                    stack: 1,
                    hideAfter: 4650,
                    afterShown: function () {
                        jQuery('.jq-toast-single .shipping-progress-bar').width(progressValue + '%');
                        setTimeout(function () {
                            if (progressValue === 100) {
                                jQuery('.jq-toast-single .shipping-progress-bar').addClass('fully-bar');
                                kontigo.shake('.jq-toast-single .shipping-delivery-ico');
                            }
                        }, 1500);
                    }
                });

            } else {
                jQuery.toast({
                    heading: heading,
                    text: '<div class="jq-toast-wrapper"><div class="toast-notice">' + text + '</div></div>',
                    position: 'top-right',
                    stack: 1,
                    hideAfter: 2500
                });
            }
        };

        cart.load = function (url) {
            sidebarService.open('cart');
            cart.setLoader(true);
            $http({
                url: url + IS_AJAX,
                method: "GET",
                responseType: "json"
            }).success(function (response) {
                cart.response(response);
            });
        };

        cart.loadWithoutSidebarOpen = function (url) {
            $http({
                url: url,
                method: "GET",
                responseType: "json"
            }).success(function (response) {
                if (!response.error) {
                    cart.refreshContents(response);
                }
                if (response.msg && response.msg.length) {
                    cart.setMessage(response.msg);
                }
            });
        };

        cart.refreshContents = function (response) {
            cart.products = response.products;
            cart.empty = response.products.length || response.products.length === 0;
            cart.totals = response.totals;
            cart.shipping = response.shipping;
            cart.discount = response.discount;
            cart.qty = response.qtyCartSession;
            cart.paymentFees = response.paymentFees;
            cart.setLoader(false);

            if (isCheckoutPage) {
                cart.setCheckoutCartBlocks(
                    response.items,
                    response.totals_block,
                    response.freeshipping,
                    response.empty_cart
                );
            }
        };

        cart.setMessage = function (string) {
            cart.message = string;
        };

        cart.remove = function (item) {
            cart.setLoader(true);
            $http({
                url: item.attr('href') + IS_AJAX,
                method: "GET"
            }).success(function (response) {
                cart.response(response);
            });
        };

        cart.response = function (response) {
            if (response && !response.error) {
                cart.refreshContents(response);
            } else {
                cart.setLoader(false);
            }
            if (response && response.msg && response.msg.length) {
                cart.setMessage(response.msg);
            }
        };

        cart.setLoader = function (bool) {
            cart.loader = bool;
        };

        cart.setItems = function (html) {
            cart.items = html;
        };

        cart.getItems = function () {
            return cart.items;
        };

        cart.setTotalsBlock = function (html) {
            cart.totals_block = html;
        };

        cart.getTotalsBlock = function () {
            return cart.totals_block;
        };

        cart.setCheckoutCartBlocks = function (items, totals, freeshipping, empty_cart) {
            cart.checkoutCartBlocks = {
                items: items,
                totals_block: totals,
                freeshipping: freeshipping,
                empty_cart: empty_cart
            };
        };

        cart.getCheckoutCartBlocks = function () {
            return cart.checkoutCartBlocks;
        };

        cart.calculateShippingProgress = function () {

            return cart.totals / cart.freeShippingData * 100;
        };

        cart.toFreeShippingValue = function () {

            return parseFloat(Math.abs(cart.totals - cart.freeShippingData)).toFixed(2);
        };

        cart.prepareToastTemplate = function (responseText, progressDetails) {

            var htmlContent =
                '<div class="jq-toast-wrapper">' +
                '<div class="toast-notice">' + responseText + '</div>' +
                '<div class="toast-shipping-content">' +
                '<span class="shipping-progress-info">' + progressDetails + '</span>' +
                '<span class="shipping-progress-container">' +
                '<span class="shipping-progress-bar"></span>' +
                '</span>' +
                '<span class="shipping-delivery-ico"><span></span></span>' +
                '</div>' +
                '</div>';

            return htmlContent;
        };

        return cart;
    }
    ]);

    app.service('sidebarService', [function () {

        var sidebar = {};
        sidebar.isOpen = false;
        sidebar.isMobileOpen = false;
        sidebar.activeContent = null;

        sidebar.close = function () {
            this.isOpen = false;
        };

        sidebar.open = function (block) {
            this.isOpen = true;
            if (block) {
                sidebar.load(block);
            }
        };

        sidebar.load = function (block) {
            this.activeContent = block;
        };

        return sidebar;
    }]);

})();
/**
 * @category    Mana
 * @package     Mana_Core
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://www.manadev.com/license  Proprietary License
 */
var Mana=Mana||{};!function(e,t,n){e.extend(Mana,{_singletons:{},_defines:{jquery:e,prototype:t},define:function(e,t,n){var o=Mana._resolveDependencyNames(t);return Mana._define(e,o.names,function(){return n.apply(this,Mana._resolveDependencies(arguments,o.deps))})},require:function(e,t){var n=Mana._resolveDependencyNames(e);return Mana._define(null,n.names,function(){return t.apply(this,Mana._resolveDependencies(arguments,n.deps))})},_define:function(t,n,o){var i=[];e.each(n,function(e,t){i.push(Mana._resolveDefine(t))});var r=o.apply(this,i);return t&&(Mana._defines[t]=r),r},_resolveDefine:function(e){return Mana._defines[e]===n&&console.warn("'"+e+"' is not defined"),Mana._defines[e]},requireOptional:function(e,t){var n=Mana._resolveDependencyNames(e);return Mana._define(null,n.names,function(){return t.apply(this,Mana._resolveDependencies(arguments,n.deps))})},_resolveDependencyNames:function(t){var n=[],o=[];return e.each(t,function(e,t){var i=t.indexOf(":"),r={name:t,resolver:""};-1!=i&&(r={name:t.substr(i+1),resolver:t.substr(0,i)}),Mana._resolveDependencyName(r),n.push(r.name),o.push(r)}),{names:n,deps:o}},_resolveDependencies:function(t,n){return e.each(t,function(e,o){t[e]=Mana._resolveDependency(n[e],o)}),t},_resolveDependencyName:function(){},_resolveDependency:function(e,t){if(t!==n)switch(e.resolver){case"singleton":return Mana._singletons[e.name]===n&&(Mana._singletons[e.name]=new t),Mana._singletons[e.name]}return t}})}(jQuery,$);var m_object_initializing=!1;!function(undefined){var fnTest=/xyz/.test(function(){})?/\b_super\b/:/.*/;Mana.Object=function(){},Mana.Object.extend=function(className,prop){prop===undefined&&(prop=className,className=undefined);var _super=this.prototype;m_object_initializing=!0;var prototype=new this;m_object_initializing=!1;for(var name in prop)prototype[name]="function"==typeof prop[name]&&"function"==typeof _super[name]&&fnTest.test(prop[name])?function(e,t){return function(){var n=this._super;this._super=_super[e];var o=t.apply(this,arguments);return this._super=n,o}}(name,prop[name]):prop[name];var Object;return className===undefined?Object=function(){!m_object_initializing&&this._init&&this._init.apply(this,arguments)}:eval("Object = function "+className.replace(/\//g,"_")+"() { if (!m_object_initializing && this._init) this._init.apply(this, arguments); };"),Object.prototype=prototype,Object.prototype.constructor=Object,Object.extend=arguments.callee,Object}}(),Mana.define("Mana/Core",["jquery"],function(e){return Mana.Object.extend("Mana/Core",{getClasses:function(e){return e.className&&e.className.split?e.className.split(/\s+/):[]},getPrefixedClass:function(t,n){var o="";return e.each(this.getClasses(t),function(e,t){return 0==t.indexOf(n)?(o=t.substr(n.length),!1):void 0}),o},arrayRemove:function(e,t,n){var o=e.slice((n||t)+1||e.length);return e.length=0>t?e.length+t:t,e.push.apply(e,o)},getBlockAlias:function(e,t){var n;return 0===(n=t.indexOf(e+"-"))?t.substr((e+"-").length):t},count:function(t){var n=0;return e.each(t,function(){n++}),n},isFunction:function(e){return!!(e&&e.constructor&&e.call&&e.apply)},isString:function(e){return"[object String]"==Object.prototype.toString.call(e)}})}),Mana.define("Mana/Core/Config",["jquery"],function(e){return Mana.Object.extend("Mana/Core/Config",{_init:function(){this._data={debug:!1,showOverlay:!0,showWait:!0}},getData:function(e){return this._data[e]},setData:function(e,t){return this._data[e]=t,this},set:function(t){return e.extend(this._data,t),this},getBaseUrl:function(e){return 0==e.indexOf(this.getData("url.base"))?this.getData("url.base"):this.getData("url.secureBase")}})}),Mana.define("Mana/Core/Json",["jquery","singleton:Mana/Core"],function(e,t){return Mana.Object.extend("Mana/Core/Json",{parse:function(t){return e.parseJSON(t)},stringify:function(e){return Object.toJSON(e)},decodeAttribute:function(n){if(t.isString(n)){var o=n.split('"'),i=[];e.each(o,function(e,t){i.push(t.replace(/'/g,'"'))});var r=i.join("'");return this.parse(r)}return n}})}),Mana.define("Mana/Core/Utf8",[],function(){return Mana.Object.extend("Mana/Core/Utf8",{decode:function(e){var t=[],n=0,o=0,i=0,r=0,a=0;for(e+="";n<e.length;)i=e.charCodeAt(n),128>i?(t[o++]=String.fromCharCode(i),n++):i>191&&224>i?(r=e.charCodeAt(n+1),t[o++]=String.fromCharCode((31&i)<<6|63&r),n+=2):(r=e.charCodeAt(n+1),a=e.charCodeAt(n+2),t[o++]=String.fromCharCode((15&i)<<12|(63&r)<<6|63&a),n+=3);return t.join("")}})}),Mana.define("Mana/Core/Base64",["singleton:Mana/Core/Utf8"],function(e){return Mana.Object.extend("Mana/Core/Base64",{encode:function(e){for(var t,n,o="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",i="",r=e.length,a=0;r-->0;){if(t=e.charCodeAt(a++),i+=o.charAt(t>>2&63),r--<=0){i+=o.charAt(t<<4&63),i+="==";break}if(n=e.charCodeAt(a++),i+=o.charAt(63&(t<<4|n>>4&15)),r--<=0){i+=o.charAt(n<<2&63),i+="=";break}t=e.charCodeAt(a++),i+=o.charAt(63&(n<<2|t>>6&3)),i+=o.charAt(63&t)}return i},decode:function(t){var n,o,i,r,a,c,s,u,l="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",d=0,h=0,p="",f=[];if(!t)return t;t+="";do r=l.indexOf(t.charAt(d++)),a=l.indexOf(t.charAt(d++)),c=l.indexOf(t.charAt(d++)),s=l.indexOf(t.charAt(d++)),u=r<<18|a<<12|c<<6|s,n=u>>16&255,o=u>>8&255,i=255&u,f[h++]=64==c?String.fromCharCode(n):64==s?String.fromCharCode(n,o):String.fromCharCode(n,o,i);while(d<t.length);return p=f.join(""),p=e.decode(p)}})}),Mana.define("Mana/Core/UrlTemplate",["singleton:Mana/Core/Base64","singleton:Mana/Core/Config"],function(e,t){return Mana.Object.extend("Mana/Core/UrlTemplate",{decodeAttribute:function(n){return t.getData("debug")?n:e.decode(n.replace(/-/g,"+").replace(/_/g,"/").replace(/,/g,"="))},encodeAttribute:function(t){return e.encode(t).replace(/\+/g,"-").replace(/\//g,"_").replace(/=/g,",")}})}),Mana.define("Mana/Core/StringTemplate",["jquery"],function(e,t){return Mana.Object.extend("Mana/Core/StringTemplate",{concat:function(n,o){var i="";return e.each(n,function(e,n){var r=n[0],a=n[1];"string"==r?i+=a:"var"==r&&(i+=o[a]!==t?o[a]:"{{"+a+"}}")}),i}})}),Mana.define("Mana/Core/Layout",["jquery","singleton:Mana/Core"],function(e,t,n){return Mana.Object.extend("Mana/Core/Layout",{_init:function(){this._pageBlock=null},getPageBlock:function(){return this._pageBlock},getBlock:function(e){return this._getBlockRecursively(this.getPageBlock(),e)},getBlockForElement:function(e){var t=this._getElementBlockInfo(e);return t?this.getBlock(t.id):null},_getBlockRecursively:function(t,n){if(t.getId()==n)return t;var o=this,i=null;return e.each(t.getChildren(),function(e,t){return i=o._getBlockRecursively(t,n),i?!1:!0}),i},beginGeneratingBlocks:function(e){var t={parentBlock:e,namedBlocks:{}};return e&&(e.trigger("unload",{},!1,!0),e.trigger("unbind",{},!1,!0),t.namedBlocks=this._removeAnonymousBlocks(e)),t},endGeneratingBlocks:function(t){var o=t.parentBlock,i=t.namedBlocks,r=this;this._collectBlockTypes(o?o.getElement():document,function(t){if(!r._pageBlock){var a=document.body,c=e(a),s=c.attr("data-m-block"),u=s?t[s]:t["Mana/Core/PageBlock"];r._pageBlock=(new u).setElement(e("body")[0]).setId("page")}var l=o===n;l&&(o=r.getPageBlock()),r._generateBlocksInElement(o.getElement(),o,t,i),e.each(i,function(e,t){t.parent.removeChild(t.child)}),o.trigger("bind",{},!1,!0),o.trigger("load",{},!1,!0)})},_collectBlockTypes:function(t,n){var o=["Mana/Core/PageBlock"];this._collectBlockTypesInElement(t,o),Mana.requireOptional(o,function(){var t=arguments,i={};e.each(o,function(e,n){i[n]=t[e]}),n(i)})},_collectBlockTypesInElement:function(t,n){var o=this;e(t).children().each(function(){var e=o._getElementBlockInfo(this);e&&-1==n.indexOf(e.typeName)&&n.push(e.typeName),o._collectBlockTypesInElement(this,n)})},_removeAnonymousBlocks:function(t){var n=this,o={};return e.each(t.getChildren().slice(0),function(e,i){i.getId()?(o[i.getId()]={parent:t,child:i},n._removeAnonymousBlocks(i)):t.removeChild(i)}),o},_getElementBlockInfo:function(n){var o,i,r=e(n);return(o=t.getPrefixedClass(n,"mb-"))||(i=r.attr("data-m-block"))||r.hasClass("m-block")?{id:o||n.id,typeName:i||r.attr("data-m-block")||"Mana/Core/Block"}:null},_generateBlocksInElement:function(t,n,o,i){var r=this;e(t).children().each(function(){var e=r._createBlockFromElement(this,n,o,i);r._generateBlocksInElement(this,e||n,o,i)})},_createBlockFromElement:function(e,n,o,i){var r=this._getElementBlockInfo(e);if(r){var a,c=o[r.typeName],s=!1;return r.id?(a=n.getChild(t.getBlockAlias(n.getId(),r.id)),a?(s=!0,delete i[r.id]):c?a=new c:console.error("Block '"+r.typeName+"' is not defined"),a&&a.setId(r.id)):c?a=new c:console.error("Block '"+r.typeName+"' is not defined"),a?(a.setElement(e),s||n.addChild(a),a):null}return null},preparePopup:function(o){if(o=this._preparePopupOptions(o),o.$popup===n){var i=e("#m-popup");i.css({width:"auto",height:"auto"}),t.isString(o.content)?i.html(o.content):i.html(e(o.content).html()),o.popup["class"]&&i.addClass(o.popup["class"]),o.$popup=i}return o.$popup},_preparePopupOptions:function(e){return e.overlay===n&&(e.overlay={}),e.overlay.opacity===n&&(e.overlay.opacity=.2),e.popup===n&&(e.popup={}),e.popup.blockName===n&&(e.popup.blockName="Mana/Core/PopupBlock"),e.popupBlock===n&&(e.popupBlock={}),e.fadein===n&&(e.fadein={}),e.fadein.overlayTime===n&&(e.fadein.overlayTime=0),e.fadein.popupTime===n&&(e.fadein.popupTime=300),e.fadeout===n&&(e.fadeout={}),e.fadeout.overlayTime===n&&(e.fadeout.overlayTime=0),e.fadeout.popupTime===n&&(e.fadeout.popupTime=500),e},showPopup:function(t){t=this._preparePopupOptions(t);var n=this;Mana.requireOptional([t.popup.blockName],function(o){var i=t.fadeout.callback;t.fadeout.callback=function(){e("#m-popup").fadeOut(t.fadeout.popupTime,function(){i&&i()})};var r=n.getPageBlock().showOverlay("m-popup-overlay",t.fadeout),a=n.preparePopup(t);r.animate({opacity:t.overlay.opacity},t.fadein.overlayTime,function(){a.show();var i=new o;i.setElement(a[0]);var r=n.beginGeneratingBlocks(i);n.endGeneratingBlocks(r),i.prepare&&i.prepare(t.popupBlock),e(".m-popup-overlay").on("click",function(){return n.hidePopup(),!1}),n._popupEscListenerInitialized||(n._popupEscListenerInitialized=!0,e(document).keydown(function(t){return e(".m-popup-overlay").length&&27==t.keyCode?(n.hidePopup(),!1):!0})),a.css("top",(e(window).height()-a.outerHeight())/2+e(window).scrollTop()+"px").css("left",(e(window).width()-a.outerWidth())/2+e(window).scrollLeft()+"px");a.height();a.hide().css({height:"auto"});var c={left:a.css("left"),top:a.css("top"),width:a.width()+"px",height:a.height()+"px"};a.children().each(function(){e(this).css({width:a.width()+e(this).width()-e(this).outerWidth()+"px",height:a.height()+e(this).height()-e(this).outerHeight()+"px"})}),a.css({top:e(window).height()/2+e(window).scrollTop()+"px",left:e(window).width()/2+e(window).scrollLeft()+"px",width:"0px",height:"0px"}).show(),a.animate(c,t.fadein.popupTime,t.fadein.callback)})})},hidePopup:function(){this.getPageBlock().hideOverlay()}})}),Mana.define("Mana/Core/Ajax",["jquery","singleton:Mana/Core/Layout","singleton:Mana/Core/Json","singleton:Mana/Core","singleton:Mana/Core/Config"],function(e,t,n,o,i,r){return Mana.Object.extend("Mana/Core/Ajax",{_init:function(){this._interceptors=[],this._matchedInterceptorCache={},this._lastAjaxActionSource=r,this._oldSetLocation=r,this._preventClicks=0},_encodeUrl:function(e,t){return t.encode?t.encode.offset!==r?t.encode.length===r?0===t.encode.offset?window.encodeURI(e.substr(t.encode.offset)):e.substr(0,t.encode.offset)+window.encodeURI(e.substr(t.encode.offset)):0===t.encode.length?e:0===t.encode.offset?window.encodeURI(e.substr(t.encode.offset,t.encode.length))+e.substr(t.encode.offset+t.encode.length):e.substr(0,t.encode.offset)+window.encodeURI(e.substr(t.encode.offset,t.encode.length))+e.substr(t.encode.offset+t.encode.length):e:window.encodeURI(e)},get:function(t,n,o){var i=this;o=this._before(o,t),e.get(this._encodeUrl(t,o)).done(function(e){i._done(e,n,o,t)}).fail(function(e){i._fail(e,o,t)}).complete(function(){i._complete(o,t)})},post:function(t,n,o,i){var a=this;n===r&&(n=[]),o===r&&(o=function(){}),i=this._before(i,t,n),e.post(window.encodeURI(t),n).done(function(e){a._done(e,o,i,t,n)}).fail(function(e){a._fail(e,i,t,n)}).complete(function(){a._complete(i,t,n)})},update:function(n){n.updates&&e.each(n.updates,function(t,n){e(t).html(n)}),n.config&&i.set(n.config),n.blocks&&e.each(n.blocks,function(e,o){var i=t.getBlock(e);i&&i.setContent(n.sections[o])}),n.script&&e.globalEval(n.script),n.title&&(document.title=n.title.replace(/&amp;/g,"&"))},getSectionSeparator:function(){return"\n91b5970cd70e2353d866806f8003c1cd56646961\n"},_before:function(n,o){var i=t.getPageBlock();return n=e.extend({showOverlay:i.getShowOverlay(),showWait:i.getShowWait(),showDebugMessages:i.getShowDebugMessages()},n),n.showOverlay&&i.showOverlay(),n.showWait&&i.showWait(),n.preventClicks&&this._preventClicks++,e(document).trigger("m-ajax-before",[[],o,"",n]),n},_done:function(e,o,i,r,a){var c=t.getPageBlock();i.showOverlay&&c.hideOverlay(),i.showWait&&c.hideWait();try{var s=e;try{var u=e.split(this.getSectionSeparator());e=u.shift(),e=n.parse(e),e.sections=u}catch(l){return o(s,{url:r}),void 0}e?e.error&&!e.customErrorDisplay?i.showDebugMessages&&alert(e.message||e.error):o(e,{url:r,data:a}):i.showDebugMessages&&alert("No response.")}catch(d){if(i.showDebugMessages){var h="";"string"==typeof d?h+=d:(h+=d.message,d.fileName&&(h+="\n    in "+d.fileName+" ("+d.lineNumber+")")),e&&(h+="\n\n",h+="string"==typeof e?e:n.stringify(e)),alert(h)}}},_fail:function(e,n){var o=t.getPageBlock();n.showOverlay&&o.hideOverlay(),n.showWait&&o.hideWait(),n.showDebugMessages&&e.status>0&&alert(e.status+(e.responseText?": "+e.responseText:""))},_complete:function(t,n){t.preventClicks&&this._preventClicks--,e(document).trigger("m-ajax-after",[[],n,"",t])},addInterceptor:function(e){this._interceptors.push(e)},removeInterceptor:function(e){var t=this._interceptors.indexOf(e);-1!=t&&this._interceptors.splice(t,1)},startIntercepting:function(){var t=this;window.History&&window.History.enabled&&e(window).on("statechange",t._onStateChange=function(){var e=window.History.getState(),n=e.url;t._findMatchingInterceptor(n,t._lastAjaxActionSource)?t._internalCallInterceptionCallback(n,t._lastAjaxActionSource):t._oldSetLocation(n,t._lastAjaxActionSource)}),window.setLocation&&(this._oldSetLocation=window.setLocation,window.setLocation=function(e,n){t._callInterceptionCallback(e,n)}),e(document).on("click","a",t._onClick=function(){var e=this.href;return t._preventClicks&&e==location.href+"#"?!1:t._findMatchingInterceptor(e,this)?t._callInterceptionCallback(e,this):!0})},stopIntercepting:function(){window.History&&window.History.enabled&&(e(window).off("statechange",self._onStateChange),self._onStateChange=null),e(document).off("click","a",self._onClick),self._onClick=null},_internalCallInterceptionCallback:function(e,t){var n=this._findMatchingInterceptor(e,t);return n?(this.lastUrl=e,n.intercept(e,t),!1):!0},_callInterceptionCallback:function(e,t){return this._findMatchingInterceptor(e,t)?(this._lastAjaxActionSource=t,window.History&&window.History.enabled?window.History.pushState(null,window.title,e):this._internalCallInterceptionCallback(e,t)):this._oldSetLocation(e,t),!1},_findMatchingInterceptor:function(t,n){if(this._matchedInterceptorCache[t]===r){var o=!1;i.getData("ajax.enabled")&&e.each(this._interceptors,function(e,i){return i.match(t,n)?(o=i,!1):!0}),this._matchedInterceptorCache[t]=o}return this._matchedInterceptorCache[t]},getDocumentUrl:function(){return this.lastUrl?this.lastUrl:document.URL}})}),Mana.define("Mana/Core/Block",["jquery","singleton:Mana/Core","singleton:Mana/Core/Layout","singleton:Mana/Core/Json"],function(e,t,n,o,i){return Mana.Object.extend("Mana/Core/Block",{_init:function(){this._id="",this._element=null,this._parent=null,this._children=[],this._namedChildren={},this._isSelfContained=!1,this._eventHandlers={},this._data={},this._text={},this._subscribeToHtmlEvents()._subscribeToBlockEvents()},_subscribeToHtmlEvents:function(){return this._json={},this},_subscribeToBlockEvents:function(){return this},getElement:function(){return this._element},$:function(){return e(this.getElement())},setElement:function(e){return this._element=e,this},addChild:function(e){return this._children.push(e),e.getId()&&(this._namedChildren[t.getBlockAlias(this.getId(),e.getId())]=e),e._parent=this,this},removeChild:function(n){var o=e.inArray(n,this._children);return-1!=o&&(t.arrayRemove(this._children,o),n.getId()&&delete this._namedChildren[t.getBlockAlias(this.getId(),n.getId())]),n._parent=null,this},getIsSelfContained:function(){return this._isSelfContained},setIsSelfContained:function(e){return this._isSelfContained=e,this},getId:function(){return this._id||this.getElement().id},setId:function(e){return this._id=e,this},getParent:function(){return this._parent},getChild:function(n){if(t.isFunction(n)){var o=null;return e.each(this._children,function(e,t){return n(e,t)?(o=t,!1):!0}),o}return this._namedChildren[n]},getChildren:function(t){if(t===i)return this._children.slice(0);var n=[];return e.each(this._children,function(e,o){t(e,o)&&n.push(o)}),n},getAlias:function(){var t=i,n=this;return e.each(this._parent._namedChildren,function(e,o){return o===n?(t=e,!1):!0}),t},_trigger:function(t,n){return n.stopped||this._eventHandlers[t]===i||e.each(this._eventHandlers[t],function(e,t){var o=t.callback.call(t.target,n);return o===!1&&(n.stopped=!0),o}),n.result},trigger:function(t,n,o,r){return n===i&&(n={}),n.target===i&&(n.target=this),r===i&&(r=!1),o===i&&(o=!0),this._trigger(t,n),r&&e.each(this.getChildren(),function(e,o){o.trigger(t,n,!1,r)}),o&&this.getParent()&&this.getParent().trigger(t,n,o,!1),n.result},on:function(e,t,n,o){return this._eventHandlers[e]===i&&(this._eventHandlers[e]=[]),o===i&&(o=0),this._eventHandlers[e].push({target:t,callback:n,sortOrder:o}),this._eventHandlers[e].sort(function(e,t){return e.sortOrder<t.sortOrder?-1:e.sortOrder>t.sortOrder?1:0}),this},off:function(n,o,r){this._eventHandlers[n]===i&&(this._eventHandlers[n]=[]);var a=-1;e.each(this._eventHandlers[n],function(e,t){return t.target==o&&t.callback==r?(a=e,!1):!0}),-1!=a&&t.arrayRemove(this._eventHandlers[n],a)},setContent:function(t){if("string"!=e.type(t)){if(!(t.content&&this.getId()&&t.content[this.getId()]))return this;t=t.content[this.getId()]}var o=n.beginGeneratingBlocks(this);return t=e(t),e(this.getElement()).replaceWith(t),this.setElement(t[0]),n.endGeneratingBlocks(o),this},getData:function(e){return this._data[e]},setData:function(e,t){return this._data[e]=t,this},getText:function(e){return this._text[e]===i&&(this._text[e]=this.$().data(e+"-text")),this._text[e]},getJsonData:function(e,t){return this._json[e]===i&&(this._json[e]=o.decodeAttribute(this.$().data(e))),t===i?this._json[e]:this._json[e][t]}})}),Mana.define("Mana/Core/PopupBlock",["jquery","Mana/Core/Block","singleton:Mana/Core/Layout"],function(e,t,n){return t.extend("Mana/Core/PopupBlock",{prepare:function(e){var t=this;this._host=e.host,this.$().find(".btn-close").on("click",function(){return t._close()})},_close:function(){return n.hidePopup(),!1}})}),Mana.define("Mana/Core/PageBlock",["jquery","Mana/Core/Block","singleton:Mana/Core/Config"],function(e,t,n){return t.extend("Mana/Core/PageBlock",{_init:function(){this._defaultOverlayFadeout={overlayTime:0,popupTime:0,callback:null},this._overlayFadeout=this._defaultOverlayFadeout,this._super()},_subscribeToHtmlEvents:function(){function t(){o||(o=!0,n.resize(),o=!1)}var n=this,o=!1;return this._super().on("bind",this,function(){e(window).on("load",t),e(window).on("resize",t)}).on("unbind",this,function(){e(window).off("load",t),e(window).off("resize",t)})},_subscribeToBlockEvents:function(){return this._super().on("load",this,function(){this.resize()})},resize:function(){this.trigger("resize",{},!1,!0)},showOverlay:function(t,n){this._overlayFadeout=n||this._defaultOverlayFadeout;var o=t?e('<div class="m-overlay '+t+'"></div>'):e('<div class="m-overlay"></div>');return o.appendTo(this.getElement()),o.css({left:0,top:0}).width(e(document).width()).height(e(document).height()),o},hideOverlay:function(){var t=this;return e(".m-overlay").fadeOut(this._overlayFadeout.overlayTime,function(){e(".m-overlay").remove(),t._overlayFadeout.callback&&t._overlayFadeout.callback(),t._overlayFadeout=t._defaultOverlayFadeout}),this},showWait:function(){return e("#m-wait").show(),this},hideWait:function(){return e("#m-wait").hide(),this},getShowDebugMessages:function(){return n.getData("debug")},getShowOverlay:function(){return n.getData("showOverlay")},getShowWait:function(){return n.getData("showWait")}})}),Mana.require(["jquery","singleton:Mana/Core/Layout","singleton:Mana/Core/Ajax"],function(e,t,n){function o(){var e=t.beginGeneratingBlocks();t.endGeneratingBlocks(e)}e(function(){o(),n.startIntercepting()})}),Mana.require(["jquery"],function(e){var t={xsmall:479,small:599,medium:770,large:979,xlarge:1199};Mana.rwdIsMobile=!1,e(function(){window.enquire&&window.enquire.register&&enquire.register("screen and (max-width: "+t.medium+"px)",{match:function(){Mana.rwdIsMobile=!0,e(document).trigger("m-rwd-mobile")},unmatch:function(){Mana.rwdIsMobile=!1,e(document).trigger("m-rwd-wide")}})})}),function(e){var t={},n={};e.__=function(n){if("string"==typeof n){var o=arguments;return o[0]=t[n]?t[n]:n,e.vsprintf(o)}t=e.extend(t,n)},e.options=function(t){return"string"==typeof t?n[t]:(n=e.extend(!0,n,t),e(document).trigger("m-options-changed"),void 0)},e.dynamicUpdate=function(t){t&&e.each(t,function(t,n){e(n.selector).html(n.html)})},e.dynamicReplace=function(t,n,o){t&&e.each(t,function(t,i){var r=e(t);if(r.length){var a=e(r[0]);r.length>1&&r.slice(1).remove(),a.replaceWith(o?e.utf8_decode(i):i)}else if(n)throw"There is no content to replace."})},e.errorUpdate=function(t,n){t||(t="#messages");var o=e(t);o.length?o.html('<ul class="messages"><li class="error-msg"><ul><li>'+n+"</li></ul></li></ul>"):alert(n)},e.arrayRemove=function(e,t,n){var o=e.slice((n||t)+1||e.length);return e.length=0>t?e.length+t:t,e.push.apply(e,o)},e.mViewport=function(){var e="CSS1Compat"==document.compatMode;return{l:window.pageXOffset||(e?document.documentElement.scrollLeft:document.body.scrollLeft),t:window.pageYOffset||(e?document.documentElement.scrollTop:document.body.scrollTop),w:window.innerWidth||(e?document.documentElement.clientWidth:document.body.clientWidth),h:window.innerHeight||(e?document.documentElement.clientHeight:document.body.clientHeight)}},e.mStickTo=function(t,n){var o=e(t).offset(),i=e.mViewport(),r=o.top+t.offsetHeight,a=o.left+(t.offsetWidth-n.outerWidth())/2;r+n.outerHeight()>i.t+i.h&&(r=o.top-n.outerHeight()),a+n.outerWidth()>i.l+i.w&&(a=o.left+t.offsetWidth-n.outerWidth()),n.css({left:a+"px",top:r+"px"})},e.fn.mMarkAttr=function(e,t){return this.prop(e,t),this},e(function(){try{window.mainNav&&window.mainNav("nav",{show_delay:"100",hide_delay:"100"})}catch(e){}}),e.base64_decode=function(t){var n,o,i,r,a,c,s,u,l="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",d=0,h=0,p="",f=[];if(!t)return t;t+="";do r=l.indexOf(t.charAt(d++)),a=l.indexOf(t.charAt(d++)),c=l.indexOf(t.charAt(d++)),s=l.indexOf(t.charAt(d++)),u=r<<18|a<<12|c<<6|s,n=u>>16&255,o=u>>8&255,i=255&u,f[h++]=64==c?String.fromCharCode(n):64==s?String.fromCharCode(n,o):String.fromCharCode(n,o,i);while(d<t.length);return p=f.join(""),p=e.utf8_decode(p)},e.utf8_decode=function(e){var t=[],n=0,o=0,i=0,r=0,a=0;for(e+="";n<e.length;)i=e.charCodeAt(n),128>i?(t[o++]=String.fromCharCode(i),n++):i>191&&224>i?(r=e.charCodeAt(n+1),t[o++]=String.fromCharCode((31&i)<<6|63&r),n+=2):(r=e.charCodeAt(n+1),a=e.charCodeAt(n+2),t[o++]=String.fromCharCode((15&i)<<12|(63&r)<<6|63&a),n+=3);return t.join("")};var o={overlayTime:500,popupTime:1e3,callback:null};e.mSetPopupFadeoutOptions=function(e){o=e},e.fn.extend({mPopup:function(t,n){var o=e.extend({fadeOut:{overlayTime:0,popupTime:500,callback:null},fadeIn:{overlayTime:0,popupTime:500,callback:null},overlay:{opacity:.2},popup:{contentSelector:"."+t+"-text",containerClass:"m-"+t+"-popup-container",top:100}},n);e(document).on("click",this,function(){if(e.mPopupClosing())return!1;var t=e(o.popup.contentSelector).html();e.mSetPopupFadeoutOptions(o.fadeOut);var n=e('<div class="m-popup-overlay"> </div>');return n.appendTo(document.body),n.css({left:0,top:0}).width(e(document).width()).height(e(document).height()),n.animate({opacity:o.overlay.opacity},o.fadeIn.overlayTime,function(){e("#m-popup").css({width:"auto",height:"auto"}).html(t).addClass(o.popup.containerClass).css("top",(e(window).height()-e("#m-popup").outerHeight())/2-o.popup.top+e(window).scrollTop()+"px").css("left",(e(window).width()-e("#m-popup").outerWidth())/2+e(window).scrollLeft()+"px");e("#m-popup").height();e("#m-popup").show().height(0),e("#m-popup").hide().css({height:"auto"});var n={left:e("#m-popup").css("left"),top:e("#m-popup").css("top"),width:e("#m-popup").width()+"px",height:e("#m-popup").height()+"px"};e("#m-popup").children().each(function(){e(this).css({width:e("#m-popup").width()+e(this).width()-e(this).outerWidth()+"px",height:e("#m-popup").height()+e(this).height()-e(this).outerHeight()+"px"})}),e("#m-popup").css({top:e(window).height()/2-o.popup.top+e(window).scrollTop()+"px",left:e(window).width()/2+e(window).scrollLeft()+"px",width:"0px",height:"0px"}).show(),e("#m-popup").animate(n,o.fadeIn.popupTime,function(){o.fadeIn.callback&&o.fadeIn.callback()})}),!1})}});var i=!1;e.mPopupClosing=function(e){return void 0!==e&&(i=e),i},e.mClosePopup=function(){return e.mPopupClosing(!0),e(".m-popup-overlay").fadeOut(o.overlayTime,function(){e(".m-popup-overlay").remove(),e("#m-popup").fadeOut(o.popupTime,function(){o.callback&&o.callback(),e.mPopupClosing(!1)})}),!1}}(jQuery);
//# sourceMappingURL=scripts.js.map
