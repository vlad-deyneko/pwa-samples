var CACHE_NAME = 'crystal-pwa-cache';
var urlsToCache = [
    'style.css',
    '/js/script1.js',
    '/js/script2.js',
    '/js/script3.js',
    '/js/script4.js',
    '/images/jurassic-park-001.gif',
    '/images/jurassic-park-002.gif',
    '/images/jurassic-park-003.gif',
    '/images/jurassic-park-004.gif',
    '/images/jurassic-park-005.gif',
    '/images/jurassic-park-006.gif',
    '/images/jurassic-park-007.gif',
    '/images/jurassic-park-008.gif'
];

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function (cache) {
                console.log('Opened cache.');
                return cache.addAll(urlsToCache);
            })
    );
    console.log('Service Worker has been installed in event type: ', event.type);
});

self.addEventListener('activate', (event) => {
    console.log('Service Worker has been activated in event type: ', event.type);
});

self.addEventListener('fetch', (event) => {
    console.log('Fetching: ', event.request.url);
    event.respondWith(
        caches.match(event.request)
            .then(function (response) {
                if (response) {
                    return response;
                }
                return fetch(event.request);
            })
    );
});

self.addEventListener('sync', (event) => {
    console.log('Received a sync event', event);

    const options = {
        title: 'Hello FWC!',
        body: 'How are you? We are getting into PWA now! What a feeling...',
        icon: '/images/bell-ring.png',
        tag: 'tag-for-fwc-sync-notification'
    };

    event.waitUntil(
        self.registration.showNotification(options.title, options)
    );
});

self.addEventListener('push', (event) => {
    console.log('Received a push event', event);

    const options = {
        title: 'I got a message for you!',
        body: 'Here is the body of the message',
        icon: '/images/bell-ring.png',
        tag: 'tag-for-this-notification',
    };

    event.waitUntil(
        self.registration.showNotification(title, options)
    );
});
