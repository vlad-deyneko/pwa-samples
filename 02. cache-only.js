const CACHE_NAME = 'crystal-pwa-cache';
var urlsToCache = [
    'style.css',
    '/js/script1.js',
    '/js/script2.js',
    '/js/script3.js',
    '/js/script4.js',
    '/Images/crystal-logo-alternative.png',
    '/Images/crystal-logo.png',
    '/Images/crystal-phone.png',
    '/Images/crystal-windows-store.png',
    '/Images/dog.png'
];

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function (cache) {
                console.log('Opened cache.');
                return cache.addAll(urlsToCache);
            })
    );
    console.log('Service Worker has been installed in event type: ', event.type);
});

// On fetch - get data from cache
self.addEventListener('fetch', (event) =>
    event.respondWith(fromCache(event.request))
);

function fromCache(request) {
    return caches.open(CACHE_NAME).then((cache) =>
      cache.match(request)
          .then((matching) => matching || Promise.reject('no-match')) // to do: check response status
    );
}
