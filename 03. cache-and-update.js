const CACHE_NAME = 'crystal-pwa-cache';
var urlsToCache = [
    'style.css',
    '/js/script1.js',
    '/js/script2.js',
    '/js/script3.js',
    '/js/script4.js',
    '/images/jurassic-park-001.gif',
    '/images/jurassic-park-002.gif',
    '/images/jurassic-park-003.gif',
    '/images/jurassic-park-004.gif',
    '/images/jurassic-park-005.gif',
    '/images/jurassic-park-006.gif',
    '/images/jurassic-park-007.gif',
    '/images/jurassic-park-008.gif'
];

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function (cache) {
                console.log('Opened cache.');
                return cache.addAll(urlsToCache);
            })
    );
    console.log('Service Worker has been installed in event type: ', event.type);
});

// On fetch - get data from cache, then update data from network
self.addEventListener('fetch', function(event) {
    // Respond imediately from cache
    event.respondWith(fromCache(event.request));

    // Wait for worker to end his task
    event.waitUntil(update(event.request));
});

function fromCache(request) {
    return caches.open(CACHE_NAME).then((cache) =>
        cache.match(request).then((matching) =>
            matching || Promise.reject('no-match')
        ));
}

function update(request) {
    return caches.open(CACHE_NAME).then((cache) =>
        fetch(request).then((response) =>
            cache.put(request, response)
        )
    );
}
